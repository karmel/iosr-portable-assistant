package pl.edu.agh.iosr.pa.api.response.users;

import pl.edu.agh.iosr.pa.api.entity.Patient;
import pl.edu.agh.iosr.pa.api.response.BasicResponse;

public class GetPatientDataResponse extends BasicResponse {

	private static final long serialVersionUID = -5476275715636729290L;

	private Patient patient;

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	
	
}
