package pl.edu.agh.iosr.pa.api.request.users;

import pl.edu.agh.iosr.pa.api.entity.Patient;
import pl.edu.agh.iosr.pa.api.request.Request;

public class AddPatientRequest extends Request {
	
	private static final long serialVersionUID = -7298733176227937661L;

	private Patient patientData;


	public Patient getPatientData() {
		return patientData;
	}


	public void setPatientData(Patient patientData) {
		this.patientData = patientData;
	}
}
