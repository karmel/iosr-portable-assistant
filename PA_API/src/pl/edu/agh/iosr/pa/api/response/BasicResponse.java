package pl.edu.agh.iosr.pa.api.response;

import java.io.Serializable;

public class BasicResponse implements Serializable {

	private static final long serialVersionUID = 4035251192388451672L;
	
	private Result result;
    private String message;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
