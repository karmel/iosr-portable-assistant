package pl.edu.agh.iosr.pa.api.request;

import java.io.Serializable;

import pl.edu.agh.iosr.pa.api.context.ContextData;

public abstract class Request  implements Serializable {
	
	private static final long serialVersionUID = -8522436064642557378L;
	
	private ContextData contextData;

	public ContextData getContextData() {
		return contextData;
	}

	public void setContextData(ContextData contextData) {
		this.contextData = contextData;
	}

}
