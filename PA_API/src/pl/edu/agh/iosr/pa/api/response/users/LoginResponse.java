package pl.edu.agh.iosr.pa.api.response.users;

import pl.edu.agh.iosr.pa.api.entity.Doctor;
import pl.edu.agh.iosr.pa.api.entity.Patient;
import pl.edu.agh.iosr.pa.api.response.BasicResponse;

public class LoginResponse extends BasicResponse {

	private static final long serialVersionUID = -8289542897536080555L;

	private Patient patient;
	
	private Doctor doctor;

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}
}
