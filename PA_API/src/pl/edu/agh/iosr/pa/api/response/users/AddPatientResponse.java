package pl.edu.agh.iosr.pa.api.response.users;

import pl.edu.agh.iosr.pa.api.entity.Patient;
import pl.edu.agh.iosr.pa.api.response.BasicResponse;

public class AddPatientResponse extends BasicResponse {

	private static final long serialVersionUID = 4428463221817177147L;

	
	private Patient patient;


	public Patient getPatient() {
		return patient;
	}


	public void setPatient(Patient patient) {
		this.patient = patient;
	}
}
