package pl.edu.agh.iosr.pa.api.request.users;

import pl.edu.agh.iosr.pa.api.request.Request;

public class GetPatientDataRequest extends Request {

	private static final long serialVersionUID = -4887574072228920442L;
	
	private long patientId;

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

}
