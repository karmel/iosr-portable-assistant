package pl.edu.agh.iosr.pa.api.response.alarms;

import java.util.List;

import pl.edu.agh.iosr.pa.api.entity.Alert;
import pl.edu.agh.iosr.pa.api.response.BasicResponse;

public class GetAlertsResponse extends BasicResponse {

	private static final long serialVersionUID = 286032467590841150L;
	
	private List <Alert> alerts;

	public List <Alert> getAlerts() {
		return alerts;
	}

	public void setAlerts(List <Alert> alerts) {
		this.alerts = alerts;
	}

}
