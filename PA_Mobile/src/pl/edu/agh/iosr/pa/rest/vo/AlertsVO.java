package pl.edu.agh.iosr.pa.rest.vo;

import java.util.List;

import pl.edu.agh.iosr.pa.mobile.model.AlertVO;


public class AlertsVO {

	private List<AlertVO> alerts;

	public List<AlertVO> getAlerts() {
		return alerts;
	}

	public void setAlerts(List<AlertVO> alerts) {
		this.alerts = alerts;
	}
}
