package pl.edu.agh.iosr.pa.mobile.model;

import java.util.List;


public class AddDietsVO {

	private List<DietVO> diets;

	private Long patientId;

	public List<DietVO> getDiets() {
		return diets;
	}

	public void setDiets(List<DietVO> diets) {
		this.diets = diets;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

}
