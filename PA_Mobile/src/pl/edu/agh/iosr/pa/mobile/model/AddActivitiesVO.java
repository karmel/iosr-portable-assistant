package pl.edu.agh.iosr.pa.mobile.model;

import java.util.List;


public class AddActivitiesVO {


	private List<ActivityVO> activities;

	private Long patientId;

	public List<ActivityVO> getActivities() {
		return activities;
	}

	public void setActivities(List<ActivityVO> activities) {
		this.activities = activities;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}
}
