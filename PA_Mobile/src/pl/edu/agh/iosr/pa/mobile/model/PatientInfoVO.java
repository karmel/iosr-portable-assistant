package pl.edu.agh.iosr.pa.mobile.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;



public class PatientInfoVO {


	private PatientVO patient;

	@SerializedName("activity")
	private List<ActivityVO> activities;

	private List<AlertVO> alerts;

    @SerializedName("criticalValue")
	private List<CriticalValueVO> criticalValues;

@SerializedName("diet")
	private List<DietVO> diets;

	@SerializedName("examination")
	private List<ExaminationVO> examinations;

    @SerializedName("medicamentPatient")
	private List<MedicamentPatientVO> medicaments;

@SerializedName("visit")
	private List<VisitVO> visits;

	public PatientVO getPatient() {
		return patient;
	}

	public void setPatient(PatientVO patient) {
		this.patient = patient;
	}

	public List<ActivityVO> getActivities() {
		return activities;
	}

	public void setActivities(List<ActivityVO> activities) {
		this.activities = activities;
	}

	public List<AlertVO> getAlerts() {
		return alerts;
	}

	public void setAlerts(List<AlertVO> alerts) {
		this.alerts = alerts;
	}

	public List<CriticalValueVO> getCriticalValues() {
		return criticalValues;
	}

	public void setCriticalValues(List<CriticalValueVO> criticalValues) {
		this.criticalValues = criticalValues;
	}

	public List<DietVO> getDiets() {
		return diets;
	}

	public void setDiets(List<DietVO> diets) {
		this.diets = diets;
	}

	public List<ExaminationVO> getExaminations() {
		return examinations;
	}

	public void setExaminations(List<ExaminationVO> examinations) {
		this.examinations = examinations;
	}

	public List<MedicamentPatientVO> getMedicaments() {
		return medicaments;
	}

	public void setMedicaments(List<MedicamentPatientVO> medicaments) {
		this.medicaments = medicaments;
	}

	public List<VisitVO> getVisits() {
		return visits;
	}

	public void setVisits(List<VisitVO> visits) {
		this.visits = visits;
	}


}
