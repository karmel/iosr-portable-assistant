package pl.edu.agh.iosr.pa.mobile.model;

import com.google.gson.annotations.SerializedName;

public class DoctorVO {

	private Long id;

	private String firstName;

	private String lastName;

	private SpecializationVO specialization;

	private String address;

	private String phone;

	private String login;

	private String password;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public SpecializationVO getSpecialization() {
		return specialization;
	}

	public void setSpecialization(SpecializationVO specialization) {
		this.specialization = specialization;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
