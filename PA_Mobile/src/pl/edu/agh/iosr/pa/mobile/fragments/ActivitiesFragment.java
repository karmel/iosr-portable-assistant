package pl.edu.agh.iosr.pa.mobile.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.edu.agh.iosr.pa.mobile.MainActivity;
import pl.edu.agh.iosr.pa.mobile.R;
import pl.edu.agh.iosr.pa.mobile.comunication.RestClient;
import pl.edu.agh.iosr.pa.mobile.comunication.SimpleAsyncTask;
import pl.edu.agh.iosr.pa.mobile.model.ActivityTypeVO;
import pl.edu.agh.iosr.pa.mobile.model.ActivityVO;
import pl.edu.agh.iosr.pa.mobile.model.AddActivitiesVO;
import pl.edu.agh.iosr.pa.mobile.model.ExaminationVO;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class ActivitiesFragment extends Fragment {

	private ListView listview;

	private HashMap<String, ActivityTypeVO> typesMap;

	private AutoCompleteTextView actv;

	private View rootView;

	private String[] activityTypes;

	private ArrayList<HashMap<String, String>> aList;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
				
        rootView = inflater.inflate(R.layout.activities_fragment, container, false);    
        
       actv = (AutoCompleteTextView)	rootView.findViewById(R.id.type);
       listview = (ListView) rootView.findViewById(R.id.activitiesListView);

        Map<String,ActivityTypeVO> typesMap;

        String[] from = { "flag","txt","cur" };
        int[] to = { R.id.flag,R.id.txt,R.id.cur};
        SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), aList, R.layout.list_item_layout, from, to);
 
        listview.setAdapter(adapter);
        
 
		actv.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, activityTypes));
        
        
        return rootView;
    	
    	
    }
	
	public void setActivityTypes(List<ActivityTypeVO> list){
		
		activityTypes = new String[list.size()];
		int i =0;
		typesMap = new HashMap<String, ActivityTypeVO>();
		for (ActivityTypeVO type : list){
			activityTypes[i]=type.getDescription();
			typesMap.put(type.getDescription(), type);
			i++;
		}
		
	}
	
	public void loadData(final List<ActivityVO> results) {

		aList = new ArrayList<HashMap<String, String>>();

		for (ActivityVO p : results) {
			HashMap<String, String> hm = new HashMap<String, String>();

			hm.put("txt", p.getStartDate() + " - " + p.getEndDate() + " - " + p.getActivityType().getDescription());
			hm.put("cur", "Frequency: " + p.getFrequency());
			hm.put("flag", Integer.toString(R.drawable.medical_record));
			aList.add(hm);
		}

	}

	public void addActivity() {
		
		final AddActivitiesVO req = new AddActivitiesVO();
		String type = ((EditText)rootView.findViewById(R.id.type)).getText().toString();;
		String freq = ((EditText)rootView.findViewById(R.id.frequency)).getText().toString();
		String start = ((EditText)rootView.findViewById(R.id.startDate)).getText().toString();
		String end = ((EditText)rootView.findViewById(R.id.endDate)).getText().toString();
		
		req.setPatientId(((MainActivity) getActivity()).getCurrentPatient().getPatient().getId());
		ActivityVO act = new ActivityVO();
		act.setActivityType(typesMap.get(type));
		act.setStartDate(start);
		act.setEndDate(end);
		act.setFrequency(freq);
		req.setActivities(new ArrayList<ActivityVO>());
		req.getActivities().add(act);
		
		SimpleAsyncTask t = new SimpleAsyncTask() {
			
			@Override
			protected void run() throws Exception {
				
				RestClient.getInstance().addActivity(req);
				
			}
		};
		
		t.execute(getActivity(), false);
		
		HashMap<String, String> hm = new HashMap<String, String>();

		hm.put("txt", act.getStartDate() + " - " + act.getEndDate() + " - " + act.getActivityType().getDescription());
		hm.put("cur", "Frequency: " + act.getFrequency());
		hm.put("flag", Integer.toString(R.drawable.medical_record));
		aList.add(hm);
		
		((SimpleAdapter)listview.getAdapter()).notifyDataSetChanged();
	}
}
