package pl.edu.agh.iosr.pa.mobile.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pl.edu.agh.iosr.pa.mobile.MainActivity;
import pl.edu.agh.iosr.pa.mobile.R;
import pl.edu.agh.iosr.pa.mobile.comunication.RestClient;
import pl.edu.agh.iosr.pa.mobile.comunication.SimpleAsyncTask;
import pl.edu.agh.iosr.pa.mobile.model.ActivityTypeVO;
import pl.edu.agh.iosr.pa.mobile.model.ActivityVO;
import pl.edu.agh.iosr.pa.mobile.model.AddActivitiesVO;
import pl.edu.agh.iosr.pa.mobile.model.AddExaminationVO;
import pl.edu.agh.iosr.pa.mobile.model.ExaminationTypeVO;
import pl.edu.agh.iosr.pa.mobile.model.ExaminationVO;
import pl.edu.agh.iosr.pa.mobile.model.PatientVO;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class ExaminationsFragment extends Fragment {

	private String[] examinationTypes;
	private HashMap<String, ExaminationTypeVO> typesMap;
	private AutoCompleteTextView actv;
	private ListView listview;
	private ArrayList<HashMap<String, String>> aList;
	private View rootView;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.examinations_fragment,
				container, false);

		actv = (AutoCompleteTextView) rootView
				.findViewById(R.id.activityNameField);

		listview = (ListView) rootView.findViewById(R.id.examinationsListView);

		String[] from = { "flag", "txt", "cur" };
		int[] to = { R.id.flag, R.id.txt, R.id.cur };
		SimpleAdapter adapter = new SimpleAdapter(getActivity()
				.getBaseContext(), aList, R.layout.list_item_layout, from, to);

		listview.setAdapter(adapter);

		ArrayAdapter a = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line, examinationTypes);
		actv.setAdapter(a);

		return rootView;

	}

	public void setExaminationsTypes(List<ExaminationTypeVO> list) {

		examinationTypes = new String[list.size()];
		int i = 0;
		typesMap = new HashMap<String, ExaminationTypeVO>();
		for (ExaminationTypeVO type : list) {
			examinationTypes[i] = type.getName();
			typesMap.put(type.getName(), type);
			i++;
		}

	}

	public void loadData(final List<ExaminationVO> results) {

		aList = new ArrayList<HashMap<String, String>>();

		for (ExaminationVO p : results) {
			HashMap<String, String> hm = new HashMap<String, String>();

			hm.put("txt", p.getDate() + " " + p.getExaminationType().getName());
			hm.put("cur", "Value: " + p.getValue());
			hm.put("flag", Integer.toString(R.drawable.medical_record));
			aList.add(hm);
		}

	}

	public void addExamination() {
		
		final AddExaminationVO req = new AddExaminationVO();
		String type = actv.getText().toString();;
		String date = ((EditText)rootView.findViewById(R.id.dateField)).getText().toString();
		String value = ((EditText)rootView.findViewById(R.id.valueField)).getText().toString();
		
		req.setPatientId(((MainActivity) getActivity()).getCurrentPatient().getPatient().getId());
		ExaminationVO act = new ExaminationVO();
		act.setDate(date);
		act.setExaminationType(typesMap.get(type));
		act.setValue(Integer.valueOf(value));
		req.setExamination(act);
		
		SimpleAsyncTask t = new SimpleAsyncTask() {
			
			@Override
			protected void run() throws Exception {
				
				RestClient.getInstance().addExamination(req);
				
			}
		};
		
		t.execute(getActivity(), false);
		
		HashMap<String, String> hm = new HashMap<String, String>();

		hm.put("txt", act.getDate() + " " + act.getExaminationType().getName());
		hm.put("cur", "Value: " + act.getValue());
		hm.put("flag", Integer.toString(R.drawable.medical_record));
		aList.add(hm);
		
		((SimpleAdapter)listview.getAdapter()).notifyDataSetChanged();
	}
		


}
