package pl.edu.agh.iosr.pa.mobile.model;

import java.util.Date;

import com.google.gson.annotations.SerializedName;


public class ExaminationVO {
@SerializedName("@id")
	private Long id;

	private PatientVO patient;

	private String date;
@SerializedName("@value")
	private Integer value;

	private ExaminationTypeVO examinationType;

	private VisitVO visit;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PatientVO getPatient() {
		return patient;
	}

	public void setPatient(PatientVO patient) {
		this.patient = patient;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public ExaminationTypeVO getExaminationType() {
		return examinationType;
	}

	public void setExaminationType(ExaminationTypeVO examinationType) {
		this.examinationType = examinationType;
	}

	public VisitVO getVisit() {
		return visit;
	}

	public void setVisit(VisitVO visit) {
		this.visit = visit;
	}

}
