package pl.edu.agh.iosr.pa.mobile.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.google.gson.annotations.SerializedName;

//@JsonRootName(value="ExaminationTypeVO")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExaminationTypeVO {

	@JsonProperty("id")
	private Long id;

	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
