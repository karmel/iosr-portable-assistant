package pl.edu.agh.iosr.pa.mobile.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonRootName;


public class ExaminationTypesVO {

	private List<ExaminationTypeVO> examinationTypes;

	public List<ExaminationTypeVO> getExaminationTypes() {
		return examinationTypes;
	}

	public void setExaminationTypes(List<ExaminationTypeVO> examinationTypes) {
		this.examinationTypes = examinationTypes;
	}
}
