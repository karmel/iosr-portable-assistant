package pl.edu.agh.iosr.pa.mobile.fragments;

import pl.edu.agh.iosr.pa.mobile.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class LoginFragment extends Fragment {

	   private View rootView;

	@Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
					
	        rootView = inflater.inflate(R.layout.login_fragment, container, false);
	        return rootView;    	
	    }

	public String getLogin() {
		String login = "";
		login = ((EditText) rootView.findViewById(R.id.loginField)).getText().toString();
		return login;
	}

	public String getPassword() {
		String password = "";
		password = ((EditText) rootView.findViewById(R.id.passwordField)).getText().toString();
		return password;
	}
	
	public void clean(){
		((EditText) rootView.findViewById(R.id.passwordField)).setText("");
	}
}
