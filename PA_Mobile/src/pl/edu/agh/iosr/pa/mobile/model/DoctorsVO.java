package pl.edu.agh.iosr.pa.mobile.model;

import java.util.List;


public class DoctorsVO {


	private List<DoctorVO> doctors;

	public List<DoctorVO> getDoctors() {
		return doctors;
	}

	public void setDoctors(List<DoctorVO> doctors) {
		this.doctors = doctors;
	}


}
