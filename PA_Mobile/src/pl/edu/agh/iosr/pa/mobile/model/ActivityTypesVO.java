package pl.edu.agh.iosr.pa.mobile.model;


import java.util.List;


public class ActivityTypesVO {

	private List<ActivityTypeVO> activityTypes;

	public List<ActivityTypeVO> getActivityTypes() {
		return activityTypes;
	}

	public void setActivityTypes(List<ActivityTypeVO> activityTypes) {
		this.activityTypes = activityTypes;
	}
}
