package pl.edu.agh.iosr.pa.mobile.model;

import java.util.Date;


public class MedicamentPatientVO {

	private Long id;


	private PatientVO patient;


	private MedicamentVO medicament;

	
	private String value;

	
	private String frequency;

	
	private String startDate;

	
	private String endDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PatientVO getPatient() {
		return patient;
	}

	public void setPatient(PatientVO patient) {
		this.patient = patient;
	}

	public MedicamentVO getMedicament() {
		return medicament;
	}

	public void setMedicament(MedicamentVO medicament) {
		this.medicament = medicament;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
