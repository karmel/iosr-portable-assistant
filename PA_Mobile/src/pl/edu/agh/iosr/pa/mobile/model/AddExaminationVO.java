package pl.edu.agh.iosr.pa.mobile.model;


public class AddExaminationVO {

	private ExaminationVO examination;

	private Long patientId;

	public ExaminationVO getExamination() {
		return examination;
	}

	public void setExamination(ExaminationVO examination) {
		this.examination = examination;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

}
