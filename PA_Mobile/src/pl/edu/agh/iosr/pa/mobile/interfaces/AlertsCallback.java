package pl.edu.agh.iosr.pa.mobile.interfaces;

public interface AlertsCallback {

	void onAlertsDownloaded();
	
}
