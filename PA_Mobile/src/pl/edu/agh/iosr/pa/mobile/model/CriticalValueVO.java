package pl.edu.agh.iosr.pa.mobile.model;

public class CriticalValueVO {

	private Long id;

	private PatientVO patient;

	private ExaminationTypeVO examinationType;

	private Long minValue;

	private Long maxValue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PatientVO getPatient() {
		return patient;
	}

	public void setPatient(PatientVO patient) {
		this.patient = patient;
	}

	public ExaminationTypeVO getExaminationType() {
		return examinationType;
	}

	public void setExaminationType(ExaminationTypeVO examinationType) {
		this.examinationType = examinationType;
	}

	public Long getMinValue() {
		return minValue;
	}

	public void setMinValue(Long minValue) {
		this.minValue = minValue;
	}

	public Long getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Long maxValue) {
		this.maxValue = maxValue;
	}

}
