package pl.edu.agh.iosr.pa.mobile.interfaces;

import java.util.List;

import pl.edu.agh.iosr.pa.mobile.model.PatientVO;

public interface PatientSearchCallbacks {
	
	void searchCompleted(List<PatientVO> results);

}
