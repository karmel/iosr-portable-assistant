package pl.edu.agh.iosr.pa.mobile.interfaces;

import pl.edu.agh.iosr.pa.mobile.model.PatientInfoVO;

public interface PatientDataCallbacks {

	public void onDataDownloaded(PatientInfoVO info);
	
}
