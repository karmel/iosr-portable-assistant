package pl.edu.agh.iosr.pa.mobile.comunication;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;


public abstract class SimpleAsyncTask {

    private static final String TAG = "PA.AsyncTask";
    private ProgressDialog progressDialog;

    public void execute(Activity activity, boolean withProgressBar) {
        try {
            if (withProgressBar) {
                //progress bar operations
            }
            execute(activity);
        } catch (Exception e) {
            Log.v(TAG, e.getMessage());
        }
    }

    private void execute(final Activity activity) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    SimpleAsyncTask.this.run();
                } catch (Exception e) {
                	e.printStackTrace();
                //    Log.v(TAG, e.getMessage());
                } finally {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                }
                return null;
            }
        }.execute();
    }

    protected abstract void run() throws Exception;
}