package pl.edu.agh.iosr.pa.mobile.model;

import java.util.Date;


public class ActivityVO {

	private Long id;

	private PatientVO patient;


	private ActivityTypeVO activityType;

	
	private String frequency;

	
	private String startDate;

	
	private String endDate;

	private boolean done;

	
	private String duration;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PatientVO getPatient() {
		return patient;
	}

	public void setPatient(PatientVO patient) {
		this.patient = patient;
	}

	public ActivityTypeVO getActivityType() {
		return activityType;
	}

	public void setActivityType(ActivityTypeVO activityType) {
		this.activityType = activityType;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}


}
