package pl.edu.agh.iosr.pa.mobile.fragments;

import pl.edu.agh.iosr.pa.mobile.R;
import pl.edu.agh.iosr.pa.mobile.model.PatientInfoVO;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PatientDataFragment extends Fragment {

    private View rootView;
	private PatientInfoVO patient;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
				
        rootView = inflater.inflate(R.layout.patientdata_fragment, container, false);
       ((TextView) rootView.findViewById(R.id.nameView)).setText(patient.getPatient().getFirstName() +" "+ patient.getPatient().getLastName());;
       ((TextView) rootView.findViewById(R.id.loginView)).setText(patient.getPatient().getLogin());
       ((TextView)  rootView.findViewById(R.id.addressView)).setText(patient.getPatient().getAddress());
       ((TextView) rootView.findViewById(R.id.phoneView)).setText(patient.getPatient().getPhone());
       ((TextView)  rootView.findViewById(R.id.doctorName)).setText(patient.getPatient().getDoctor().getFirstName() + " " + patient.getPatient().getDoctor().getFirstName() );
        
        return rootView;	
    }

	public void loadData(PatientInfoVO info) {
		patient = info;
		
	}
	
	
}
