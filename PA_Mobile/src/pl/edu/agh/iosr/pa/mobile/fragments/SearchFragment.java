package pl.edu.agh.iosr.pa.mobile.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pl.edu.agh.iosr.pa.mobile.MainActivity;
import pl.edu.agh.iosr.pa.mobile.R;
import pl.edu.agh.iosr.pa.mobile.comunication.RestClient;
import pl.edu.agh.iosr.pa.mobile.comunication.SimpleAsyncTask;
import pl.edu.agh.iosr.pa.mobile.interfaces.PatientSearchCallbacks;
import pl.edu.agh.iosr.pa.mobile.model.PatientVO;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;

public class SearchFragment extends Fragment implements PatientSearchCallbacks{

	
	private ListView listview;
	private EditText nameField;
	private EditText surnameField;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
				
        View rootView = inflater.inflate(R.layout.search_fragment, container, false);    
      nameField =  (EditText) rootView.findViewById(R.id.nameField);
      surnameField =  (EditText) rootView.findViewById(R.id.surnameField);
       listview = (ListView) rootView.findViewById(R.id.patientListView);

        return rootView;
    	
    }

	@Override
	public void searchCompleted(final List<PatientVO> results) {
		
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
				

				
		        List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();
		        
				for (PatientVO p : results){
					HashMap<String, String> hm = new HashMap<String,String>();
					
					hm.put("txt",p.getLastName()+" "+ p.getFirstName());
					hm.put("cur","Address: " + p.getAddress() +", Phone: "+p.getPhone());
		            hm.put("flag", Integer.toString(R.drawable.medical_record) );
		            aList.add(hm);
				}

		        String[] from = { "flag","txt","cur" };
		        int[] to = { R.id.flag,R.id.txt,R.id.cur};
		        SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), aList, R.layout.list_item_layout, from, to);
		 
		        listview.setAdapter(adapter);
		        
				listview.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
						
						final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Searching. Please wait...", true);
						final PatientVO patient = results.get(position);
						
						SimpleAsyncTask ast = new SimpleAsyncTask() {
							@Override
							protected void run() throws Exception {
												
								RestClient.getInstance().getPatientInfo(patient, (MainActivity)getActivity());
								
								dialog.dismiss();
							}
						};
						
						ast.execute(getActivity(), true);
						
						
					}
				});
				
				
			}
		});

		
	}

	public PatientVO getPatientCriteria() {
		PatientVO patient = new PatientVO();
		
		patient.setFirstName(nameField.getText().toString());
		patient.setLastName(surnameField.getText().toString());
		
		return patient;
	}


	
}
