package pl.edu.agh.iosr.pa.mobile.interfaces;

public interface LoginCallbacks {

	public void onLoginComplete(int error);
	
}
