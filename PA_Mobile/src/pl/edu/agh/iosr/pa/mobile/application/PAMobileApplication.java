package pl.edu.agh.iosr.pa.mobile.application;

import android.app.Application;
import android.content.Context;

public class PAMobileApplication extends Application{

    private static Context context;

    public void onCreate(){
        super.onCreate();
        PAMobileApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return PAMobileApplication.context;
    }
}
