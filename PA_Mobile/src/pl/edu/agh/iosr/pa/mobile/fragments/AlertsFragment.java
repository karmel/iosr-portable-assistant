package pl.edu.agh.iosr.pa.mobile.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pl.edu.agh.iosr.pa.mobile.MainActivity;
import pl.edu.agh.iosr.pa.mobile.R;
import pl.edu.agh.iosr.pa.mobile.comunication.RestClient;
import pl.edu.agh.iosr.pa.mobile.comunication.SimpleAsyncTask;
import pl.edu.agh.iosr.pa.mobile.model.AlertVO;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class AlertsFragment extends Fragment{

	
	private ArrayList<HashMap<String, String>> aList;
	private List<AlertVO> alertsBase;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
				
        View rootView = inflater.inflate(R.layout.alerts_fragment, container, false);    
        
        
        final ListView listview = (ListView) rootView.findViewById(R.id.alertsListView);
        

        String[] from = { "flag","txt","cur" };
        int[] to = { R.id.flag,R.id.txt,R.id.cur};
        SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), aList, R.layout.list_item_layout, from, to);
 
        listview.setAdapter(adapter);
        
        
        listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				

				showAlertInfo(alertsBase.get(position));
				
			}
		});
        
        return rootView;
    	
    	
    }

	public void loadData(List<AlertVO> alerts) {
		alertsBase = alerts;
        aList = new ArrayList<HashMap<String,String>>();
        
        for(AlertVO a : alerts){
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("txt",a.getPatient().getFirstName() +" "+ a.getPatient().getLastName() );
            hm.put("cur","Reason : " + a.getCause() );
            hm.put("flag", Integer.toString(R.drawable.excl) );
            aList.add(hm);
        }
		
	}
	
	private void showAlertInfo(final AlertVO a){
		new AlertDialog.Builder(getActivity())
	    .setTitle("Alert Information!")
	    .setMessage("Patient address:\n"+a.getPatient().getAddress()+"\n"+a.getPatient().getPhone()+"\n\nResponsible doctor:\n"+a.getDoctor().getFirstName() +" " + a.getDoctor().getLastName() + ", "+a.getDoctor().getPhone())
	    .setPositiveButton("OK", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		}).setNegativeButton("Consult patient", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				final ProgressDialog spinner = ProgressDialog.show(getActivity(), "", "Searching. Please wait...", true);
				
				SimpleAsyncTask task = new SimpleAsyncTask() {
					
					@Override
					protected void run() throws Exception {
						RestClient.getInstance().getPatientInfo(a.getPatient(), (MainActivity)getActivity());
						spinner.dismiss();
					}
				}; 
				
				task.execute(getActivity(), false);
				
			}
		})
	    .setIcon(android.R.drawable.ic_dialog_alert)
	    .show();
	}
	
}
