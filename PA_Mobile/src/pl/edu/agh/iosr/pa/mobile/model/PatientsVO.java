package pl.edu.agh.iosr.pa.mobile.model;

import java.util.List;


public class PatientsVO {


	private List<PatientVO> patients;

	public List<PatientVO> getPatients() {
		return patients;
	}

	public void setPatients(List<PatientVO> patients) {
		this.patients = patients;
	}


}
