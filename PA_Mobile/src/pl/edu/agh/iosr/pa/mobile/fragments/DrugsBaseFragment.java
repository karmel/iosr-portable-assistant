package pl.edu.agh.iosr.pa.mobile.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pl.edu.agh.iosr.pa.mobile.R;
import pl.edu.agh.iosr.pa.mobile.model.MedicamentVO;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class DrugsBaseFragment extends Fragment {
	private ListView listview;
	private View rootView;
	private ArrayList<HashMap<String, String>> aList;
	private List<MedicamentVO> base;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
				
        rootView = inflater.inflate(R.layout.drugs_db_fragment, container, false);    
        
        
       listview = (ListView) rootView.findViewById(R.id.drugsdbListView);

       
        

        String[] from = { "flag","txt","c"};
        int[] to = { R.id.flag,R.id.txt,R.id.cur};
        SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), aList, R.layout.list_item_layout, from, to);
 
        listview.setAdapter(adapter);
        
        listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				((TextView) rootView.findViewById(R.id.medicineDesc)).setText(base.get(position).getPurpose());
				
			}
		});
        
        return rootView;
    	
    	
    }

	public void setDrugs(List<MedicamentVO> drugsBase) {
		aList = new ArrayList<HashMap<String,String>>();
		base = drugsBase;
		
		for(MedicamentVO m : drugsBase){
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("txt",m.getName());
            hm.put("c", "");
            hm.put("flag", Integer.toString(R.drawable.pharmaceutical_drugs) );
            aList.add(hm);
        }
		
	}
	
}
