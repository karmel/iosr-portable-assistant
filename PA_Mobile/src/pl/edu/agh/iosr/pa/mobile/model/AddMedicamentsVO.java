package pl.edu.agh.iosr.pa.mobile.model;

import java.util.List;

public class AddMedicamentsVO {

	private List<MedicamentPatientVO> medicaments;

	private Long patientId;

	public List<MedicamentPatientVO> getMedicaments() {
		return medicaments;
	}

	public void setMedicaments(List<MedicamentPatientVO> medicaments) {
		this.medicaments = medicaments;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

}
