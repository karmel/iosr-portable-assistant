package pl.edu.agh.iosr.pa.mobile.model;

import java.util.List;


public class MedicamentsVO {

	private List<MedicamentVO> medicaments;

	public List<MedicamentVO> getMedicaments() {
		return medicaments;
	}

	public void setMedicaments(List<MedicamentVO> medicaments) {
		this.medicaments = medicaments;
	}
}
