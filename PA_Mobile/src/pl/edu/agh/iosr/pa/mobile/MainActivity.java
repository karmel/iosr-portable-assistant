package pl.edu.agh.iosr.pa.mobile;

import java.io.IOException;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import pl.edu.agh.iosr.pa.mobile.comunication.RestClient;
import pl.edu.agh.iosr.pa.mobile.comunication.SimpleAsyncTask;
import pl.edu.agh.iosr.pa.mobile.fragments.ActivitiesFragment;
import pl.edu.agh.iosr.pa.mobile.fragments.AlertsFragment;
import pl.edu.agh.iosr.pa.mobile.fragments.DietsFragment;
import pl.edu.agh.iosr.pa.mobile.fragments.DrugsBaseFragment;
import pl.edu.agh.iosr.pa.mobile.fragments.ExaminationsFragment;
import pl.edu.agh.iosr.pa.mobile.fragments.LoginFragment;
import pl.edu.agh.iosr.pa.mobile.fragments.PatientDataFragment;
import pl.edu.agh.iosr.pa.mobile.fragments.PatientHistoryFragment;
import pl.edu.agh.iosr.pa.mobile.fragments.SearchFragment;
import pl.edu.agh.iosr.pa.mobile.fragments.TherapyFragment;
import pl.edu.agh.iosr.pa.mobile.fragments.list.MenuListFragment;
import pl.edu.agh.iosr.pa.mobile.interfaces.LoginCallbacks;
import pl.edu.agh.iosr.pa.mobile.interfaces.PatientDataCallbacks;
import pl.edu.agh.iosr.pa.mobile.model.DoctorVO;
import pl.edu.agh.iosr.pa.mobile.model.PatientInfoVO;
import pl.edu.agh.iosr.pa.mobile.model.PatientVO;
import pl.edu.agh.iosr.pa.mobile.notifications.NotificationsActivity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

public class MainActivity extends NotificationsActivity implements
		MenuListFragment.Callbacks, LoginCallbacks, PatientDataCallbacks {

	
	private boolean isLoggedIn;

	public MainActivity self;
	
	private DoctorVO activeDoctor;
	
	private ExaminationsFragment examinationsFragment;
	private AlertsFragment alertsFragment;
	private DietsFragment dietsFragment;
	private ActivitiesFragment activitiesFragment;
	private SearchFragment searchFragment;
	private PatientHistoryFragment patientHistoryFragment;
	private LoginFragment loginFragment;
	private PatientDataFragment patientDataFragment;
	private DrugsBaseFragment drugsBaseFragment;
	private TherapyFragment therapyFragment;
	
	private PatientInfoVO currentPatient;

	private Context context;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_itembum_list);

		if (findViewById(R.id.itembum_detail_container) != null) {
			((MenuListFragment) getSupportFragmentManager()
					.findFragmentById(R.id.itembum_list))
					.setActivateOnItemClick(true);
		}
		initializeFragments();
		
		
		getSupportFragmentManager().beginTransaction()
		.replace(R.id.itembum_detail_container, loginFragment).commit();
		self = this;
		
		context = getApplicationContext();
		
//        if (checkPlayServices()) {
//            gcm = GoogleCloudMessaging.getInstance(this);
//            regid = getRegistrationId(getApplicationContext());
//
//            if (regid.isEmpty()) {
//                registerInBackground();
//            }
//        }
	}

	private void initializeFragments() {
		loginFragment = new LoginFragment();
		alertsFragment = new AlertsFragment();
		searchFragment = new SearchFragment();				
		patientDataFragment = new PatientDataFragment();
		patientHistoryFragment = new PatientHistoryFragment();
		examinationsFragment = new ExaminationsFragment();
		activitiesFragment = new ActivitiesFragment();
		drugsBaseFragment = new DrugsBaseFragment();
		dietsFragment = new DietsFragment();
		therapyFragment = new TherapyFragment();
	}

	@Override
	public void onItemSelected(String id) {
		if (isLoggedIn){
			
			if (( currentPatient == null && !id.equals("7")) ||( currentPatient == null && !id.equals("0")) ){
				replaceContent(searchFragment);
				return;
			}
			
			switch (Integer.valueOf(id)) {
			case 0:
				getSupportFragmentManager().beginTransaction()
				.replace(R.id.itembum_detail_container, alertsFragment).commit();
				break;
			case 1:
				getSupportFragmentManager().beginTransaction()
				.replace(R.id.itembum_detail_container, searchFragment).commit();
				break;
			case 2:
				replaceContent(patientDataFragment);
				break;
			case 3:
				getSupportFragmentManager().beginTransaction()
				.replace(R.id.itembum_detail_container, patientHistoryFragment).commit();
				break;
			case 4:
				getSupportFragmentManager().beginTransaction()
				.replace(R.id.itembum_detail_container, examinationsFragment).commit();
				break;
			case 5:
				getSupportFragmentManager().beginTransaction()
				.replace(R.id.itembum_detail_container, activitiesFragment).commit();
				break;
			case 6:
				replaceContent(therapyFragment);
				break;
			case 7:
				getSupportFragmentManager().beginTransaction()
				.replace(R.id.itembum_detail_container, drugsBaseFragment).commit();
				break;
			case 8:
				replaceContent(dietsFragment);
				break;
			case 9:
				if (isLoggedIn){
					isLoggedIn = false;
					currentPatient = null;
					replaceContent(loginFragment);
					loginFragment.clean();
				}
				break;
				
			default:
				break;
			}
			
			
		}else{

			getSupportFragmentManager().beginTransaction()
			.replace(R.id.itembum_detail_container, loginFragment).commit();

		}
	}
	
	public void doLogin(View v){
		
		
		
		final String login = loginFragment.getLogin();
		final String password = loginFragment.getPassword();
		
		
		if (login.isEmpty() || password.isEmpty()){
		
			showError("Login and password can not be empty!");
			
			return;
		}else{
		
			final ProgressDialog dialog = ProgressDialog.show(MainActivity.this, "", "Logging in. Please wait...", true);
			
			SimpleAsyncTask ast = new SimpleAsyncTask() {
				@Override
				protected void run() throws Exception {
					
					boolean result = RestClient.getInstance().login("06795", "haslo", self);
					
					if (result){
						
						
						self.runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								
								dialog.setMessage("Synchronizing data...Please wait...");
								
							}
						});
						RestClient.getInstance().updateGCM(regid);
						RestClient.getInstance().getDoctorData("61993", self);
						examinationsFragment.setExaminationsTypes(RestClient.getInstance().getExaminationTypes());
						activitiesFragment.setActivityTypes(RestClient.getInstance().getActivityTypes());
						drugsBaseFragment.setDrugs(RestClient.getInstance().getDrugsBase());
						alertsFragment.loadData(RestClient.getInstance().getAlerts());
						getSupportFragmentManager().beginTransaction().replace(R.id.itembum_detail_container, alertsFragment).commit();
					}
					
					dialog.dismiss();
				}
			};
			ast.execute(this, false);
		}
	}

	private void showError(final String msg) {
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
				new AlertDialog.Builder(self)
			    .setTitle("Error")
			    .setMessage(msg)
			    .setPositiveButton("OK", new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				})
			    .setIcon(android.R.drawable.ic_dialog_alert)
			    .show();
				
			}
		});
		
	}

	private void replaceContent(Fragment fr){
		getSupportFragmentManager().beginTransaction().replace(R.id.itembum_detail_container, fr).commit();
	}
	
	public void searchPatient(View v){
		
		final PatientVO criteria = searchFragment.getPatientCriteria();
		
		final ProgressDialog dialog = ProgressDialog.show(MainActivity.this, "", "Searching. Please wait...", true);
		
		SimpleAsyncTask ast = new SimpleAsyncTask() {
			@Override
			protected void run() throws Exception {
								
				RestClient.getInstance().	searchPatient(criteria, searchFragment);
				
				dialog.dismiss();
			}
		};
		ast.execute(this, false);
		
	}
	
	@Override
	public void onLoginComplete(int error) {

			if (error == RestClient.LOGIN_SUCCESS){
				isLoggedIn = true;	
				
			}else{
				
				String msg = "";
				
				if (error == RestClient.LOGIN_ERROR){
					msg = "Wrong login or password!";
				}else{
					msg = "Internal error";
				}
				
				if ( !msg.isEmpty() ){
					showError(msg);
				}
			}
		
	}
	

	public DoctorVO getActiveDoctor() {
		return activeDoctor;
	}

	public void setActiveDoctor(DoctorVO activeDoctor) {
		activeDoctor = activeDoctor;
	}

	@Override
	public void onDataDownloaded(PatientInfoVO info) {
		
		currentPatient = info;
		patientDataFragment.loadData(info);
		currentPatient.getVisits();
		patientHistoryFragment.loadData(currentPatient.getVisits());
		examinationsFragment.loadData(currentPatient.getExaminations());
		activitiesFragment.loadData(currentPatient.getActivities());
		dietsFragment.loadData(currentPatient.getDiets());
		therapyFragment.loadData(currentPatient.getMedicaments());
		
	}
	
	@Override
	protected void onResume() {
	    super.onResume();
	   // checkPlayServices();
	}
	
	public void addVisit(View v){
		patientHistoryFragment.addVisit();
	}
	
	public void addExamination(View v){
		examinationsFragment.addExamination();
	}
	
	public void addActivity(View v){
		activitiesFragment.addActivity();
	}
	
	public void addDiet(View v){
		dietsFragment.addDiet();
	}

	public PatientInfoVO getCurrentPatient() {
		return currentPatient;
	}

	public void setCurrentPatient(PatientInfoVO currentPatient) {
		this.currentPatient = currentPatient;
	}

}
