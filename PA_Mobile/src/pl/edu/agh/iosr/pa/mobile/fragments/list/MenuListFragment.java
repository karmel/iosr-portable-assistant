package pl.edu.agh.iosr.pa.mobile.fragments.list;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pl.edu.agh.iosr.pa.mobile.R;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class MenuListFragment extends ListFragment {

	private static final String STATE_ACTIVATED_POSITION = "activated_position";

    String[] entries = new String[] {
    		   "Alerts",
    		   "Search patient",
    		    "Patient data",
    		    "Patient history",
    		    "Examinations",
    		    "Activities",
    		    "Drugs",
    		    "Drugs database",
    		    "Diets",
    		    "Logout"
    };
    


    int[] icons = new int[]{
        R.drawable.excl,
        R.drawable.medical_record,
        R.drawable.patient_chart,
        R.drawable.medical_record,
        R.drawable.diagnostic_laboratory,
        R.drawable.ecg_chart,
        R.drawable.prescription_drugs,
        R.drawable.pharmaceutical_drugs,
        R.drawable.prescription_drugs,
        R.drawable.doctor
    };
 
    // Array of strings to store currencies
    String[] currency = new String[]{
        "Indian Rupee",
        "Pakistani Rupee",
        "Sri Lankan Rupee",
        "Renminbi",
        "Bangladeshi Taka",
        "Nepalese Rupee",
        "Afghani",
        "North Korean Won",
        "South Korean Won",
        "Japanese Yen"
    };
	
	private int mActivatedPosition = ListView.INVALID_POSITION;


	private Callbacks mCallbacks;

	public interface Callbacks {
		public void onItemSelected(String id);
	}


	private static Callbacks sDummyCallbacks = new Callbacks() {
		@Override
		public void onItemSelected(String id) {
		}
	};

	public MenuListFragment() {
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
 
        List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();
 
        for(int i=0;i<10;i++){
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("txt",entries[i]);
            hm.put("cur","Currency : " + currency[i]);
            hm.put("flag", Integer.toString(icons[i]) );
            aList.add(hm);
        }
 
        // Keys used in Hashmap
        String[] from = { "flag","txt","cur" };
 
        // Ids of views in listview_layout
        int[] to = { R.id.flag,R.id.txt,R.id.cur};
 
        // Instantiating an adapter to store each items
        // R.layout.listview_layout defines the layout of each item
        SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), aList, R.layout.menu_layout, from, to);
 
        setListAdapter(adapter);
 
        return super.onCreateView(inflater, container, savedInstanceState);
    }

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		if (savedInstanceState != null
				&& savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
			setActivatedPosition(savedInstanceState
					.getInt(STATE_ACTIVATED_POSITION));
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException("Activity must implement fragment's callbacks.");
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = sDummyCallbacks;
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position,long id) {
		super.onListItemClick(listView, view, position, id);
		mCallbacks.onItemSelected(new Integer(position).toString());
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
	}

	public void setActivateOnItemClick(boolean activateOnItemClick) {
		getListView().setChoiceMode(
				activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
						: ListView.CHOICE_MODE_NONE);
	}

	private void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION) {
			getListView().setItemChecked(mActivatedPosition, false);
		} else {
			getListView().setItemChecked(position, true);
		}

		mActivatedPosition = position;
	}
}
