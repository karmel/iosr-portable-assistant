package pl.edu.agh.iosr.pa.mobile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ItembumDetailFragment extends Fragment {

	public static final String ARG_ITEM_ID = "item_id";

	public ItembumDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_itembum_detail,
				container, false);


		return rootView;
	}
}
