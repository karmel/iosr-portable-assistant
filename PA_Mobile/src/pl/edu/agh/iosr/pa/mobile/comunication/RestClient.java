package pl.edu.agh.iosr.pa.mobile.comunication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import pl.edu.agh.iosr.pa.mobile.MainActivity;
import pl.edu.agh.iosr.pa.mobile.application.PAMobileApplication;
import pl.edu.agh.iosr.pa.mobile.interfaces.LoginCallbacks;
import pl.edu.agh.iosr.pa.mobile.interfaces.PatientDataCallbacks;
import pl.edu.agh.iosr.pa.mobile.interfaces.PatientSearchCallbacks;
import pl.edu.agh.iosr.pa.mobile.model.ActivityTypeVO;
import pl.edu.agh.iosr.pa.mobile.model.ActivityTypesVO;
import pl.edu.agh.iosr.pa.mobile.model.AddActivitiesVO;
import pl.edu.agh.iosr.pa.mobile.model.AddExaminationVO;
import pl.edu.agh.iosr.pa.mobile.model.AlertVO;
import pl.edu.agh.iosr.pa.mobile.model.DoctorVO;
import pl.edu.agh.iosr.pa.mobile.model.ExaminationTypeVO;
import pl.edu.agh.iosr.pa.mobile.model.ExaminationTypesVO;
import pl.edu.agh.iosr.pa.mobile.model.MedicamentVO;
import pl.edu.agh.iosr.pa.mobile.model.MedicamentsVO;
import pl.edu.agh.iosr.pa.mobile.model.PatientInfoVO;
import pl.edu.agh.iosr.pa.mobile.model.PatientVO;
import pl.edu.agh.iosr.pa.mobile.model.PatientsVO;
import pl.edu.agh.iosr.pa.mobile.properties.PropertyReader;
import pl.edu.agh.iosr.pa.rest.vo.AlertsVO;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

public class RestClient {

	private static RestClient instance;

	public static final int LOGIN_ERROR = 401;
	public static final int LOGIN_SUCCESS = 200;
	public static final int UNKNOWN_ERROR = -1;
	private String serviceUrl = "https://192.168.1.106:8443/pa";
	private static String cookie;
	private ObjectMapper objectMapper;

	private RestClient() {

		Properties p = new PropertyReader(PAMobileApplication.getAppContext())
				.getProperties("web.properties");
		serviceUrl = p.getProperty("endpoint");

		objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, false);
	}

	public static RestClient getInstance() {

		if (instance == null) {
			instance = new RestClient();
		}

		return instance;
	}

	public boolean login(String login, String password, LoginCallbacks callbacks) {

		try {
			HttpClient httpclient = PortableAssistantSocketFactory
					.getNewHttpClient();
			HttpPost httpPost = new HttpPost(serviceUrl + "/auth/login");
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("j_username", login));
			params.add(new BasicNameValuePair("j_password", password));
			params.add(new BasicNameValuePair("submit", "Login"));
			httpPost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-Type",
					"application/x-www-form-urlencoded");
			HttpResponse httpResponse = httpclient.execute(httpPost);

			int status = httpResponse.getStatusLine().getStatusCode();

			if (status == 200) {
				Header header = httpResponse.getFirstHeader("Set-Cookie");
				cookie = header.getValue();
			}
			callbacks.onLoginComplete(httpResponse.getStatusLine()
					.getStatusCode());
			return status == 200 ? true : false;
		} catch (Exception e) {
			callbacks.onLoginComplete(UNKNOWN_ERROR);
			return false;
		}

	}

	public List<AlertVO> getAlerts() {

		HttpURLConnection urlConnection = null;
		List<AlertVO> resultList = new ArrayList<AlertVO>();

		try {
			HttpClient httpclient = PortableAssistantSocketFactory
					.getNewHttpClient();
			System.out.println();
			HttpGet httpGet = prepareGet(serviceUrl + "/rest/data/alerts");
			HttpResponse httpResponse = httpclient.execute(httpGet);
			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {
				InputStream instream = entity.getContent();
				String result = convertStreamToString(instream);

				instream.close();
				AlertsVO l = objectMapper.readValue(result, AlertsVO.class);
				return l.getAlerts();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return resultList;

	}

	public List<ExaminationTypeVO> getExaminationTypes() {

		HttpURLConnection urlConnection = null;
		List<ExaminationTypeVO> resultList = new ArrayList<ExaminationTypeVO>();

		try {
			HttpClient httpclient = PortableAssistantSocketFactory
					.getNewHttpClient();
			System.out.println();
			HttpGet httpGet = prepareGet(serviceUrl
					+ "/rest/data/examinationTypes");
			HttpResponse httpResponse = httpclient.execute(httpGet);
			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {
				InputStream instream = entity.getContent();
				String result = convertStreamToString(instream);

				instream.close();
				ExaminationTypesVO l = objectMapper.readValue(result,
						ExaminationTypesVO.class);
				return l.getExaminationTypes();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return resultList;

	}

	public List<MedicamentVO> getDrugsBase() {

		HttpURLConnection urlConnection = null;
		List<MedicamentVO> resultList = new ArrayList<MedicamentVO>();

		try {
			HttpClient httpclient = PortableAssistantSocketFactory
					.getNewHttpClient();
			HttpGet httpGet = prepareGet(serviceUrl + "/rest/data/medicaments");
			HttpResponse httpResponse = httpclient.execute(httpGet);
			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {
				InputStream instream = entity.getContent();
				String result = convertStreamToString(instream);

				instream.close();
				MedicamentsVO l = objectMapper.readValue(result,
						MedicamentsVO.class);
				return l.getMedicaments();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return resultList;

	}

	public List<ActivityTypeVO> getActivityTypes() {

		HttpURLConnection urlConnection = null;
		List<ActivityTypeVO> yourList = new ArrayList<ActivityTypeVO>();
		try {
			HttpClient httpclient = PortableAssistantSocketFactory
					.getNewHttpClient();
			System.out.println();
			HttpGet httpGet = prepareGet(serviceUrl
					+ "/rest/data/activityTypes");
			HttpResponse httpResponse = httpclient.execute(httpGet);

			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {
				InputStream instream = entity.getContent();
				String result = convertStreamToString(instream);
				instream.close();
				ActivityTypesVO actt = objectMapper.readValue(result,
						ActivityTypesVO.class);
				yourList = actt.getActivityTypes();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return yourList;

	}

	public void searchPatient(PatientVO criteria,
			PatientSearchCallbacks callbacks) {

		HttpClient httpclient = PortableAssistantSocketFactory
				.getNewHttpClient();
		HttpPost httpPost = preparePost(serviceUrl + "/rest/data/searchPatient");

		try {
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-Type", "application/json");

			String json = objectMapper.writeValueAsString(criteria);

			httpPost.setEntity(new StringEntity(json));

			HttpResponse httpResponse = httpclient.execute(httpPost);

			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {
				InputStream instream = entity.getContent();
				String result = convertStreamToString(instream);

				instream.close();

				PatientsVO patiens = objectMapper.readValue(result,
						PatientsVO.class);
				callbacks.searchCompleted(patiens.getPatients());

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void getPatientInfo(PatientVO patient, PatientDataCallbacks callbacks) {

		HttpURLConnection urlConnection = null;
		try {
			HttpClient httpclient = PortableAssistantSocketFactory
					.getNewHttpClient();
			HttpGet httpGet = prepareGet(serviceUrl + "/rest/patient/info/"
					+ new Float(patient.getLogin()).intValue());
			HttpResponse httpResponse = httpclient.execute(httpGet);

			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {
				InputStream instream = entity.getContent();
				String result = convertStreamToString(instream);

				instream.close();

				PatientInfoVO info = objectMapper.readValue(result,
						PatientInfoVO.class);

				callbacks.onDataDownloaded(info);
				// readValue(result, PatientInfoVO.class);
			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}

	}

	private static HttpGet prepareGet(String url) {
		HttpGet httpGet = new HttpGet(url);
		httpGet.setHeader("Accept", "application/json");
		httpGet.setHeader("Cookie", cookie);
		return httpGet;
	}

	private static HttpPost preparePost(String url) {
		HttpPost httpPost = new HttpPost(url);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Cookie", cookie);
		return httpPost;
	}

	private HttpEntity callGet(String URL) throws ClientProtocolException,
			IOException {
		HttpClient httpclient = PortableAssistantSocketFactory
				.getNewHttpClient();
		HttpGet httpGet = prepareGet(serviceUrl + URL);
		HttpResponse httpResponse = httpclient.execute(httpGet);
		return httpResponse.getEntity();
	}

	public void getDoctorData(String doctorLogin, MainActivity mainActivity) {

		HttpURLConnection urlConnection = null;
		try {
			HttpClient httpclient = PortableAssistantSocketFactory
					.getNewHttpClient();
			HttpGet httpGet = prepareGet(serviceUrl + "/rest/doctor/61993");
			HttpResponse httpResponse = httpclient.execute(httpGet);

			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {
				InputStream instream = entity.getContent();
				String result = convertStreamToString(instream);

				DoctorVO d = objectMapper.readValue(result, DoctorVO.class);
				mainActivity.setActiveDoctor(d);
			}

			System.out.println("");
		} catch (MalformedURLException e) {
			// URL is invalid
		} catch (SocketTimeoutException e) {
			// data retrieval or connection timed out
		} catch (IOException e) {
			e.printStackTrace();
			// could not read response body
			// (could not create input stream)
		} finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
	}

	private static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	public void addActivity(AddActivitiesVO req) {
		HttpClient httpclient = PortableAssistantSocketFactory
				.getNewHttpClient();
		HttpPost httpPost = preparePost(serviceUrl + "/rest/data/addActivities");

		try {
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-Type", "application/json");

			String json = objectMapper.writeValueAsString(req);

			httpPost.setEntity(new StringEntity(json));

			HttpResponse httpResponse = httpclient.execute(httpPost);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void addExamination(AddExaminationVO req) {
		HttpClient httpclient = PortableAssistantSocketFactory
				.getNewHttpClient();
		HttpPost httpPost = preparePost(serviceUrl
				+ "/rest/data/addExamination");

		try {
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-Type", "application/json");

			String json = objectMapper.writeValueAsString(req);

			httpPost.setEntity(new StringEntity(json));

			httpclient.execute(httpPost);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void updateGCM(String regid) {
		System.out.println(regid);

		HttpClient httpclient = PortableAssistantSocketFactory
				.getNewHttpClient();
		HttpPost httpPost = preparePost(serviceUrl + "/rest/gcm/" + regid);

		try {
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-Type", "application/json");
			// httpPost.setEntity(new StringEntity(json));

			httpclient.execute(httpPost);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}