package pl.edu.agh.iosr.pa.mobile.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pl.edu.agh.iosr.pa.mobile.R;
import pl.edu.agh.iosr.pa.mobile.model.DietVO;
import pl.edu.agh.iosr.pa.mobile.model.VisitVO;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class PatientHistoryFragment extends Fragment {


	private ListView listview;
	private View rootView;
	private ArrayList<HashMap<String, String>> aList;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
				
        rootView = inflater.inflate(R.layout.diets_fragment, container, false);    
        
        
       listview = (ListView) rootView.findViewById(R.id.dietsListView);

        String[] from = { "flag","txt","cur" };
        int[] to = { R.id.flag,R.id.txt,R.id.cur};
        SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), aList, R.layout.list_item_layout, from, to);
 
        listview.setAdapter(adapter);
        return rootView;
    	
    	
    }

	public void loadData(List<VisitVO> list) {
		
		aList = new ArrayList<HashMap<String,String>>();
		
		
		for (VisitVO d : list){
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("txt",d.getDate());
            hm.put("cur","Doctor: " + d.getDoctor().getFirstName() + d.getDoctor().getLastName() );
            hm.put("flag", Integer.toString(R.drawable.pharmaceutical_drugs) );
            aList.add(hm);
		}
		
		
	}

	public void addVisit() {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
