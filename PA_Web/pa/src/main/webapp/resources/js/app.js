var paApp = angular.module('paApp',
		[ 'ngRoute', 'ngSanitize', 'mm.foundation', 'http-auth-interceptor','ngResource','ngCookies', 'truncate' , 'ngAnimate', 'angularMoment', 'ngQuickDate']);

// Declare app level module which depends on filters, and services
paApp.config([ '$routeProvider', '$locationProvider', '$httpProvider', 'USER_ROLES', 
		function($routeProvider, $locationProvider, $httpProvider, USER_ROLES) {

			$routeProvider.when('/login', {
				templateUrl : 'resources/views/login.html',
				controller : 'LoginController',
				access: {
                    authorizedRoles: [USER_ROLES.all]
                }
			});

			$routeProvider.when('/logout', {
				templateUrl : 'resources/views/home.html',
				controller : 'LogoutController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/home', {
				templateUrl : 'resources/views/home.html',
				controller : 'HomeController',
				access: {
                    authorizedRoles: [USER_ROLES.all]
                }
			});

			$routeProvider.when('/activities', {
				templateUrl : 'resources/views/activities/show.html',
				controller : 'ActivitiesController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/activities/create', {
				templateUrl : 'resources/views/activities/create.html',
				controller : 'ActivitiesController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/activities/:id', {
				templateUrl : 'resources/views/activities/edit.html',
				controller : 'ActivitiesController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/alerts', {
				templateUrl : 'resources/views/alerts/show.html',
				controller : 'AlertsController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/alerts/create', {
				templateUrl : 'resources/views/alerts/create.html',
				controller : 'AlertsController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/alerts/:id', {
				templateUrl : 'resources/views/alerts/edit.html',
				controller : 'AlertsController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/diets', {
				templateUrl : 'resources/views/diets/show.html',
				controller : 'DietsController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/diets/create', {
				templateUrl : 'resources/views/diets/create.html',
				controller : 'DietsController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/diets/:id', {
				templateUrl : 'resources/views/diets/edit.html',
				controller : 'DietsController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/doctors', {
				templateUrl : 'resources/views/doctors/show.html',
				controller : 'DoctorsController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/examinations', {
				templateUrl : 'resources/views/examinations/show.html',
				controller : 'ExaminationsController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/examinations/create', {
				templateUrl : 'resources/views/examinations/create.html',
				controller : 'ExaminationsController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/examinations/:id', {
				templateUrl : 'resources/views/examinations/edit.html',
				controller : 'ExaminationsController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/medicaments', {
				templateUrl : 'resources/views/medicaments/show.html',
				controller : 'MedicamentsController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/medicaments/:id', {
				templateUrl : 'resources/views/medicaments/show_detail.html',
				controller : 'MedicamentsController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/therapy', {
				templateUrl : 'resources/views/therapy/show.html',
				controller : 'TherapyController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/visits', {
				templateUrl : 'resources/views/visits/show.html',
				controller : 'VisitsController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});

			$routeProvider.when('/settings', {
				templateUrl : 'resources/views/settings.html',
				controller : 'SettingsController',
				access: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
                }
			});
			
			$routeProvider.when('/error', {
				templateUrl : 'resources/views/error.html',
				access: {
					authorizedRoles: [USER_ROLES.all]
				}
			});

			$routeProvider.otherwise({
				redirectTo : '/home',
				access: {
					authorizedRoles: [USER_ROLES.all]
				}
			});
		} ]);
paApp.config(['ngQuickDateDefaultsProvider', function(ngQuickDateDefaultsProvider) {
	  ngQuickDateDefaultsProvider.set('parseDateFunction', function(str) {
	    d = Date.create(str);
	    return d.isValid() ? d : null;
	  });
	}]);

paApp.config(['$httpProvider', function($httpProvider){
    $httpProvider.defaults.headers.get = { 'Accept' : 'application/json' };
    $httpProvider.defaults.headers.post = { 'Content-Type' : 'application/json' };
}]);

paApp.run(['$rootScope', '$location', '$http', 'AuthenticationSharedService', 'Session', 'USER_ROLES', 'DataCacheService', 'Account', 'StorageService', '$timeout','authService', 'lastUrl', '$route',
           function($rootScope, $location, $http, AuthenticationSharedService, Session, USER_ROLES, DataCacheService, Account, StorageService, $timeout, authService, lastUrl, $route) {
	$http.get('rest/data/currentUser').then(function(data){
		$timeout(function(){
			data = angular.fromJson(data);
			roles = [];
			principal = {};
			if (data.data.doctor) {
				principal = data.data;
				roles = [ USER_ROLES.user ];
			} else {
				principal = data.data;
				roles = [ USER_ROLES.admin ];
			}
			Session.create(
					StorageService.get('currentUser'),
					principal.firstName,
					principal.lastName,
					principal.phone,
							roles);
			$rootScope.account = Session;
			$rootScope.login = StorageService.get('currentUser');
			$rootScope.id = StorageService.get('id');
			authService.loginConfirmed(principal);
			$rootScope.authenticationError = false;
			$rootScope.authenticated = true;
			StorageService.save('authenticated', $rootScope.authenticated);
			$route.reload();
		});
	});
	DataCacheService.create();
	
	$rootScope.isLoggedIn = function(){
		return StorageService.get('authenticated');
	}
	
    $rootScope.$on('$routeChangeStart', function (event, next) {
        $rootScope.isAuthorized = AuthenticationSharedService.isAuthorized;
        $rootScope.userRoles = USER_ROLES;
        AuthenticationSharedService.valid(next.access.authorizedRoles);
    });
    
    $rootScope.$on('$viewContentLoaded', function () {
        jQ(document).foundation();
    });

    // Call when the the client is confirmed
    $rootScope.$on('event:auth-loginConfirmed', function(data) {
        $rootScope.authenticated = true;
        StorageService.save('authenticated', $rootScope.authenticated);
        if ($location.path() === "/login") {
            lastUrl.redirectToAttemptedUrl();
        }
    });

    // Call when the 401 response is returned by the server
    $rootScope.$on('event:auth-loginRequired', function(rejection) {
        Session.invalidate();
        $rootScope.authenticated = false;
        StorageService.save('authenticated', $rootScope.authenticated);
        if($location.path() !== "/login"){
        	lastUrl.saveAttemptUrl();
        }
        if ($location.path() !== "/" && $location.path() !== "") {
            $location.path('/login').replace();
        }
    });

    // Call when the 403 response is returned by the server
    $rootScope.$on('event:auth-notAuthorized', function(rejection) {
        $rootScope.errorMessage = 'errors.403';
        lastUrl.saveAttemptUrl();
        $location.path('/error').replace();
    });

    // Call when the user logs out
    $rootScope.$on('event:auth-loginCancelled', function() {
        $location.path('');
    });
}]);

//where we will store the attempted url
paApp.value('redirectToUrlAfterLogin', { url: '/' });
 
//this service will be responsible for authentication and also saving and redirecting to the attempt url when logging in
 
paApp.factory('lastUrl', ['$location', '$cookies', 'redirectToUrlAfterLogin', function ($location,  $cookies, redirectToUrlAfterLogin) {
  return {
    saveAttemptUrl: function() {
      if($location.path().toLowerCase() != '/login') {
        redirectToUrlAfterLogin.url = $location.path();
      }
      else
        redirectToUrlAfterLogin.url = '/';
    },
    redirectToAttemptedUrl: function() {
      $location.path(redirectToUrlAfterLogin.url).replace();
    }
  };
}]);
