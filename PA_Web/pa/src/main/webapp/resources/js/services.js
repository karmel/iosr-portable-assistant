'use strict';

/* Services */

angular.module('paApp').factory('Account', [ '$resource', function($resource) {
	return $resource('rest/data/currentUser', {}, {});
} ]);

angular.module('paApp').factory('Session', [ function() {
	this.create = function(login, firstName, lastName, phone, userRoles) {
		this.login = login;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.userRoles = userRoles;
	};
	this.invalidate = function() {
		this.login = null;
		this.firstName = null;
		this.lastName = null;
		this.phone = null;
		this.userRoles = null;
	};
	return this;
} ]);

angular.module('paApp').constant('USER_ROLES', {
	all : '*',
	admin : 'ROLE_ADMIN',
	user : 'ROLE_USER'
});

angular.module('paApp').factory(
		'DataSharedService',
		[
				'$rootScope',
				'$http',
				'USER_ROLES',
				function($rootScope, $http, USER_ROLES) {
					return {
						getData : function(successCallback) {
							if ($rootScope.account.userRoles
									.indexOf(USER_ROLES.user) === -1) {
								console.log("You are logged in as a Doctor");
							} else {
								$http
										.get(
												'rest/patient/info/'
														+ $rootScope.login)
										.success(successCallback);
							}
						}
					};
				} ]);
angular.module('paApp').factory('DataCacheService', [ function() {
	this.create = function() {
		this.activities = null;
		this.alerts = null;
		this.diets = null;
		this.doctors = null;
		this.examinations = null;
		this.medicaments = null;
		this.therapy = null;
		this.visits = null;
	};
	this.reset = function(){
		this.create();
	};
	return this;
} ]);

angular
		.module('paApp')
		.factory(
				'AuthenticationSharedService',
				[
						'$rootScope',
						'$location',
						'$http',
						'DataSharedService',
						'DataCacheService',
						'authService',
						'Session',
						'Account',
						'USER_ROLES',
						'StorageService',
						'lastUrl',
						function($rootScope, $location, $http,
								DataSharedService, DataCacheService, authService, Session,
								Account, USER_ROLES, StorageService, lastUrl) {
							return {
								login : function(param) {
									var data = "j_username=" + param.username
											+ "&j_password=" + param.password
											+ "&submit=Login";
									$rootScope.login = param.username;
									StorageService.save('currentUser', $rootScope.login);
									$http
											.post(
													'auth/login',
													data,
													{
														headers : {
															"Content-Type" : "application/x-www-form-urlencoded"
														},
														ignoreAuthModule : 'ignoreAuthModule'
													})
											.success(
													function(data, status,
															headers, config) {
														Account
																.get(function(
																		data) {
																	data = angular
																			.fromJson(data);
																	if (data.doctor) {
																		data.roles = [ USER_ROLES.user ];
																	} else {
																		data.roles = [ USER_ROLES.admin ];
																	}
																	Session
																			.create(
																					data.login,
																					data.firstName,
																					data.lastName,
																					data.phone,
																					data.roles);
																	StorageService.save('id', data.id);
																	$rootScope.account = Session;
																	authService
																			.loginConfirmed(data);
																	$rootScope.authenticationError = false;
																	$rootScope.authenticated = true;
																	StorageService.save('authenticated', $rootScope.authenticated);
																	lastUrl.redirectToAttemptedUrl();
																});
													})
											.error(
													function(data, status,
															headers, config) {
														$rootScope.authenticationError = true;
														$rootScope.authenticated = false;
														StorageService.save('authenticated', $rootScope.authenticated);
														Session.invalidate();
														DataCacheService.reset();
														
													});
								},
								valid : function(authorizedRoles) {

									$http
											.get('protected/transparent.gif', {
											// ignoreAuthModule :
											// 'ignoreAuthModule'
											})
											.success(
													function(data, status,
															headers, config) {
														if (!Session.login) {
															Account
																	.get(function(
																			data) {
																		Session
																				.create(
																						data.login,
																						data.firstName,
																						data.lastName,
																						data.email,
																						data.roles);
																		$rootScope.account = Session;

																		if (!$rootScope
																				.isAuthorized(authorizedRoles)) {
																			event
																					.preventDefault();
																			// user
																			// is
																			// not
																			// allowed
																			$rootScope
																					.$broadcast("event:auth-notAuthorized");
																		}

																		$rootScope.authenticated = true;
																		StorageService.save('authenticated', $rootScope.authenticated);
																		
																	});
														}
														$rootScope.authenticated = true;
														StorageService.save('authenticated', $rootScope.authenticated);
													})
											.error(
													function(data, status,
															headers, config) {
														console.log(data);
													});
								},
								isAuthorized : function(authorizedRoles) {
									if (!angular.isArray(authorizedRoles)) {
										if (authorizedRoles == '*') {
											return true;
										}

										authorizedRoles = [ authorizedRoles ];
									}

									var isAuthorized = false;
									angular
											.forEach(
													authorizedRoles,
													function(authorizedRole) {
														var authorized = (!!Session.login && Session.userRoles
																.indexOf(authorizedRole) !== -1);

														if (authorized
																|| authorizedRole == '*') {
															isAuthorized = true;
														}
													});

									return isAuthorized;
								},
								logout : function() {
									$rootScope.authenticationError = false;
									$rootScope.authenticated = false;
									StorageService.save('authenticated', $rootScope.authenticated);
									$rootScope.account = null;
									$rootScope.dataFromServer = null;

									$http.get('auth/logout');
									Session.invalidate();
									DataCacheService.reset();
									authService.loginCancelled();
								}
							};
						} ]);
