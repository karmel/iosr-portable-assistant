'use strict';

angular.module('paApp').controller('ExaminationsController', ['$q', '$scope', '$http', '$routeParams', '$filter', '$rootScope', 'DataSharedService', 'DataCacheService', function($q, $scope, $http, $routeParams, $filter, $rootScope, DataSharedService, DataCacheService) {
	$scope.id = $routeParams.id;
	$scope.loadData = function(forceRefresh){
		if($rootScope.account){
			if(DataCacheService.examinations && !forceRefresh){
				$scope.examinations = DataCacheService.examinations;
				if(DataCacheService.examinationTypes){
					$scope.examinationTypes = DataCacheService.examinationTypes;
					$scope.exType = $scope.examinationTypes[0];
				}else{
					$scope.loadExaminationTypes();
				}
			}else{
				var deferred = $q.defer();
				DataSharedService.getData(function(data, status,
						headers, config){
					if(status == 200){
						DataCacheService.examinations = data.examinations;
						deferred.resolve(DataCacheService.examinations);
					}else{
						console.log('ERROR, ERROR EXAMINATIONS GET ERROR');
						deferred.reject();
					}
				});
				$scope.examinations = deferred.promise;
				deferred.promise.then(function(data){
					$scope.examinations = data;
				});
				$scope.loadExaminationTypes();
			}
		}
	};
	$scope.loadExaminationTypes = function(){
		var deferred2 = $q.defer();
		$http.get("rest/data/examinationTypes").success(function(data, status, headers, config){
			DataCacheService.examinationTypes = data.examinationTypes.filter(function(item){ 
				return item.id == 4 || item.id == 16; 
			});
			deferred2.resolve(DataCacheService.examinationTypes);
		}).error(function(data, status, headers, config){
			console.log('ERROR, ERROR EXAMINATION TYPES GET ERROR');
			deferred2.reject();
		});
		$scope.examinationTypes = deferred2.promise;
		deferred2.promise.then(function(data){
			$scope.examinationTypes = data;
			$scope.exType = data[0];
		});
	};
	$scope.loadData(false);
    $scope.add = function(){
    	$http.post("rest/data/addUserExamination/" + $scope.exType.id + "/" + $scope.exValue,{},{headers:{'Accept' : 'text/plain'}}).success(function(data, status, headers, config){
    		console.log("Success!" + data);
    		$scope.loadData(true);
    	});
    };
	$scope.sortByDate = function(){
		if($scope.currentSort == "date"){
			$scope.examinations = $filter('orderBy')($scope.examinations, "-date");
			$scope.currentSort = "-date";
		}else{
			$scope.examinations = $filter('orderBy')($scope.examinations, "date");
			$scope.currentSort = "date";
		}
	};
	$scope.sortByType = function(){
		if($scope.currentSort == "type"){
			$scope.examinations = $filter('orderBy')($scope.examinations, "-examinationType.name");
			$scope.currentSort = "-type";
		}else{
			$scope.examinations = $filter('orderBy')($scope.examinations, "examinationType.name");
			$scope.currentSort = "type";
		}
	};
}]);