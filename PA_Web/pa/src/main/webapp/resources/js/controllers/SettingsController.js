'use strict';

angular.module('paApp').controller('SettingsController', ['$scope', '$http', '$routeParams', '$filter',  '$rootScope', function($scope, $http, $routeParams, $filter, $rootScope) {
	$scope.settings = function(){
		$rootScope.account.login = $rootScope.login;
		return $rootScope.account;
	};
}]);