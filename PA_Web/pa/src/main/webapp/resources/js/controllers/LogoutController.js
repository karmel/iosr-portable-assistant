/**
 * New node file
 */
angular.module('paApp').controller('LogoutController', ['$location', 'AuthenticationSharedService',
    function ($location, AuthenticationSharedService) {
        AuthenticationSharedService.logout();
        $location.path("/").replace();
    }]);