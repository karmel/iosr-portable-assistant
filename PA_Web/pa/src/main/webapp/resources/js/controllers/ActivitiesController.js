'use strict';

angular.module('paApp').controller('ActivitiesController', ['$q', '$scope', '$http', '$routeParams' , '$filter', '$rootScope', 'DataSharedService', 'DataCacheService', function($q, $scope, $http, $routeParams, $filter, $rootScope, DataSharedService, DataCacheService) {
	$scope.id = $routeParams.id;
	$scope.userId = $rootScope.id;
	$scope.currentSort = null;
	$scope.loadData = function(forceRefresh){
		if($rootScope.account){
			if(DataCacheService.activities && !forceRefresh){
				$scope.activities = DataCacheService.activities;
				if(DataCacheService.activityTypes){
					$scope.activityTypes = DataCacheService.activityTypes;
					$scope.aType = $scope.activityTypes[0];
				}else{
					$scope.loadActivityTypes();
				}
			}else{
				var deferred = $q.defer();
				DataSharedService.getData(function(data, status,
						headers, config){
					if(status == 200){
						DataCacheService.activities = data.activities;
						deferred.resolve(DataCacheService.activities);
					}else{
						console.log('ERROR, ERROR ACTIVIIES GET ERROR');
						deferred.reject();
					}
				});
				$scope.activities = deferred.promise;
				deferred.promise.then(function(data){
					$scope.activities = data;
				});
				$scope.loadActivityTypes();
			}
		}
	};
	$scope.sortByStartDate = function(){
		if($scope.currentSort == "startDate"){
			$scope.activities = $filter('orderBy')($scope.activities, "-startDate");
			$scope.currentSort = "-startDate";
		}else{
			$scope.activities = $filter('orderBy')($scope.activities, "startDate");
			$scope.currentSort = "startDate";
		}
	};
	$scope.sortByEndDate = function(){
		if($scope.currentSort == "endDate"){
			$scope.activities = $filter('orderBy')($scope.activities, "-endDate");
			$scope.currentSort = "-endDate";
		}else{
			$scope.activities = $filter('orderBy')($scope.activities, "endDate");
			$scope.currentSort = "endDate";
		}
	};
	$scope.loadActivityTypes = function(){
		var deferred2 = $q.defer();
		$http.get("rest/data/activityTypes").success(function(data, status, headers, config){
			DataCacheService.activityTypes = data.activityTypes;
			deferred2.resolve(DataCacheService.activityTypes);
		}).error(function(data, status, headers, config){
			console.log('ERROR, ERROR EXAMINATION TYPES GET ERROR');
			deferred2.reject();
		});
		$scope.activityTypes = deferred2.promise;
		deferred2.promise.then(function(data){
			$scope.activityTypes = data;
			$scope.aType = data[0];
		});
	};
	$scope.loadData(false);
    $scope.add = function(){
    	var activitiesPayload = {
    			"patientId" : $scope.userId,
    			"activities" : [{
    				"activityType" : {
    					"id" : $scope.aType.id
    				},
					"frequency" : $scope.freq,
					"startDate" : $scope.startDate.toISOString().slice(0, 10),
					"endDate" : $scope.endDate.toISOString().slice(0, 10),
					"done" : $scope.done ? true : false,
					"duration" : $scope.duration	
    			}]
    	};
    	$http.post("rest/data/addActivities",activitiesPayload,{headers:{'Accept' : 'text/plain'}}).success(function(data, status, headers, config){
    		console.log("Success!" + data);
    		$scope.loadData(true);
    	});
    };
}]);