'use strict';

angular.module('paApp').controller('DietsController', ['$q', '$scope', '$http', '$routeParams', '$filter', '$rootScope', 'DataSharedService', 'DataCacheService', function($q, $scope, $http, $routeParams, $filter, $rootScope, DataSharedService, DataCacheService) {
	$scope.id = $routeParams.id;
	$scope.loadData = function(){
		if($rootScope.account){
			if(DataCacheService.diets){
				$scope.diets = DataCacheService.diets;
			}else{
				var deferred = $q.defer();
				DataSharedService.getData(function(data, status,
						headers, config){
					if(status == 200){
						DataCacheService.diets = data.diets;
						deferred.resolve(DataCacheService.diets);
					}else{
						console.log('ERROR, ERROR DIETS GET ERROR');
						deferred.reject();
					}
				});
				$scope.diets = deferred.promise;
				deferred.promise.then(function(data){
					$scope.diets = data;
				});
			}
		}
	};
	$scope.loadData();
	$scope.sortByStartDate = function(){
		if($scope.currentSort == "startDate"){
			$scope.diets = $filter('orderBy')($scope.diets, "-startDate");
			$scope.currentSort = "-startDate";
		}else{
			$scope.diets = $filter('orderBy')($scope.diets, "startDate");
			$scope.currentSort = "startDate";
		}
	};
	$scope.sortByEndDate = function(){
		if($scope.currentSort == "endDate"){
			$scope.diets = $filter('orderBy')($scope.diets, "-endDate");
			$scope.currentSort = "-endDate";
		}else{
			$scope.diets = $filter('orderBy')($scope.diets, "endDate");
			$scope.currentSort = "endDate";
		}
	};
}]);