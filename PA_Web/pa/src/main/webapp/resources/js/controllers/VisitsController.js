'use strict';

angular.module('paApp').controller('VisitsController', ['$q', '$scope', '$http', '$routeParams', '$filter', '$rootScope', 'DataSharedService', 'DataCacheService', function($q, $scope, $http, $routeParams, $filter, $rootScope, DataSharedService, DataCacheService) {
	$scope.loadData = function(){
		if($rootScope.account){
			if(DataCacheService.visits){
				$scope.visits = DataCacheService.visits;
			}else{
				var deferred = $q.defer();
				DataSharedService.getData(function(data, status,
						headers, config){
					if(status == 200){
						DataCacheService.visits = data.visits;
						deferred.resolve(DataCacheService.visits);
					}else{
						console.log('ERROR, ERROR VISITS GET ERROR');
						deferred.reject();
					}
				});
				$scope.visits = deferred.promise;
				deferred.promise.then(function(data){
					$scope.visits = data;
				});
			}
		}
	};
	$scope.loadData();
	$scope.sortByDate = function(){
		if($scope.currentSort == "date"){
			$scope.visits = $filter('orderBy')($scope.visits, "-date");
			$scope.currentSort = "-date";
		}else{
			$scope.visits = $filter('orderBy')($scope.visits, "date");
			$scope.currentSort = "date";
		}
	};
}]);