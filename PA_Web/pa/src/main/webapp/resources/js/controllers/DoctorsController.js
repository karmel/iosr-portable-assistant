'use strict';

angular.module('paApp').controller('DoctorsController', ['$q', '$scope', '$http', '$routeParams', '$rootScope', '$filter', 'DataCacheService', function($q, $scope, $http, $routeParams, $rootScope, $filter, DataCacheService) {
	$scope.id = $routeParams.id;
	$scope.data = DataCacheService.doctors;
	$scope.filteredData = $scope.data;
	$scope.totalItems = 10;
	$scope.totalFilteredItems = 10;
	$scope.currentPage = 1;
	$scope.itemsPerPage = 10;
	$scope.doctorFilter = "";
	$scope.loadData = function(){
		if($rootScope.account){
			if($scope.data){
				$scope.doctors = $scope.data;
				$scope.totalItems = $scope.doctors.length;
				$scope.applyFilter();
				$scope.onPageChanged(1);
			}else{
				var deferred = $q.defer();
				$http.get('rest/doctor/', { 'Accept' : 'application/json' }).success(function(data, status,
						headers, config){
				 		data = angular.fromJson(data);
						DataCacheService.doctors = data.doctors;
						deferred.resolve(DataCacheService.doctors);
				}).error(function(data, status,
						headers, config){
					deferred.reject();
				});
				$scope.doctors = deferred.promise;
				deferred.promise.then(function(data){
					$scope.doctors = data;
					$scope.data = data;
					$scope.totalItems = $scope.doctors.length;
					$scope.applyFilter();
					$scope.onPageChanged(1);
				});
			}
		}
	};
	$scope.onPageChanged = function(pageNo){
		$currentPage = pageNo;
		var start = (pageNo - 1) * $scope.itemsPerPage;
		var end = start + $scope.itemsPerPage;
		if($scope.filteredData){
			$scope.doctors = $scope.filteredData.slice(start, end);
		};
	};
	$scope.applyFilter = function(){
		$scope.filteredData = $filter('filter')($scope.data, $scope.doctorFilter);
		$scope.totalFilteredItems = $scope.filteredData ? $scope.filteredData.length : 0;
		$scope.onPageChanged(1);
	};
	$scope.loadData();
	$scope.$watch('doctorFilter', function(){
		$scope.applyFilter();
	});
}]);