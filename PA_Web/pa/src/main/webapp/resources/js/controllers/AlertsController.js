'use strict';

angular.module('paApp').controller('AlertsController', ['$q', '$scope', '$http', '$routeParams', '$filter', '$rootScope', 'DataSharedService','DataCacheService', function($q, $scope, $http, $routeParams, $filter, $rootScope, DataSharedService, DataCacheService) {
	$scope.id = $routeParams.id;
	$scope.loadData = function(){
		if($rootScope.account){
			if(DataCacheService.alerts){
				$scope.alerts = DataCacheService.alerts;
			}else{
				var deferred = $q.defer();
				DataSharedService.getData(function(data, status,
						headers, config){
					if(status == 200){
						DataCacheService.alerts = data.alerts;
						deferred.resolve(DataCacheService.alerts);
					}else{
						console.log('ERROR, ERROR ALERTS GET ERROR');
						deferred.reject();
					}
				});
				$scope.alerts = deferred.promise;
				deferred.promise.then(function(data){
					$scope.alerts = data;
				});
			}
		}
	};
	$scope.loadData();
	$scope.closeAlert = function(index) {
	    $scope.alerts.splice(index, 1);
	  };
	$scope.fireAlert = function(){
		$http.post("rest/data/fireAlert",{headers:{'Accept' : 'text/plain'}}).success(function(data, status, headers, config){
			window.alert("Alert sent! Don't worry, rescue team is coming!");
		});
	};
}]);