'use strict';

angular.module('paApp').controller('MedicamentsController', ['$q', '$scope', '$http', '$routeParams', '$filter', '$rootScope', 'DataSharedService', 'DataCacheService', function($q, $scope, $http, $routeParams, $filter, $rootScope, DataSharedService, DataCacheService) {
	$scope.id = $routeParams.id;
	$scope.loadData = function(){
		if($rootScope.account){
			if(DataCacheService.medicaments){
				$scope.medicaments = DataCacheService.medicaments;
			}else{
				var deferred = $q.defer();
				DataSharedService.getData(function(data, status,
						headers, config){
					if(status == 200){
						DataCacheService.medicaments = data.medicaments;
						deferred.resolve(DataCacheService.medicaments);
					}else{
						console.log('ERROR, ERROR MEDICAMENTS GET ERROR');
						deferred.reject();
					}
				});
				$scope.medicaments = deferred.promise;
				deferred.promise.then(function(data){
					$scope.medicaments = data;
				});
			}
		}
	};
	$scope.loadData();
}]);