'use strict';

angular.module('paApp').controller('TherapyController', ['$q', '$scope', '$http', '$routeParams', '$rootScope', '$filter', 'DataSharedService', 'DataCacheService', function($q, $scope, $http, $routeParams, $rootScope, $filter, DataSharedService, DataCacheService) {
	$scope.loadData = function(){
		if($rootScope.account){
			if(DataCacheService.therapy){
				$scope.therapy = DataCacheService.therapy;
			}else{
				var deferred = $q.defer();
				DataSharedService.getData(function(data, status,
						headers, config){
					if(status == 200){
						DataCacheService.therapy = data;
						deferred.resolve(DataCacheService.therapy);
					}else{
						console.log('ERROR, ERROR THERAPY GET ERROR');
						deferred.reject();
					}
				});
				$scope.therapy = deferred.promise;
				deferred.promise.then(function(data){
					$scope.therapy = data;
				});
			}
		}
	};
	$scope.loadData();
}]);