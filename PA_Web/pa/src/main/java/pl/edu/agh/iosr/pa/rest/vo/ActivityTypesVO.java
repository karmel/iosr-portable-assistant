package pl.edu.agh.iosr.pa.rest.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "activityTypes")
@XmlAccessorType(XmlAccessType.NONE)
public class ActivityTypesVO {

	@XmlElementRef
	private List<ActivityTypeVO> activityTypes;

	public List<ActivityTypeVO> getActivityTypes() {
		return activityTypes;
	}

	public void setActivityTypes(List<ActivityTypeVO> activityTypes) {
		this.activityTypes = activityTypes;
	}
}
