package pl.edu.agh.iosr.pa.manager;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import pl.edu.agh.iosr.pa.dao.AlertDao;
import pl.edu.agh.iosr.pa.dao.CriticalValueDao;
import pl.edu.agh.iosr.pa.model.Alert;

public class AlertManager {

	private CriticalValueDao criticalValueDao;
	private AlertDao alertDao;
	
	public void setCriticalValueDao(CriticalValueDao arg0) {
		this.criticalValueDao = arg0;
	}

	public void setAlertDao(AlertDao arg0) {
		this.alertDao = arg0;
	}
	
	@Transactional
	public List<Alert> getAlerts(){
		return alertDao.findAll(Alert.class);
	}
	
	@Transactional
	public List<Alert> getDoctorAlerts(long doctorId){
		return alertDao.getAlertsForDoctorId(doctorId);
	}
	
	@Transactional
	public Serializable triggerAlert(Alert alert){
		return alertDao.save(alert);
	}
	
	@Transactional
	public Serializable triggerValueAlert(Alert alert){
		return alertDao.save(alert);
	}

	
}
