package pl.edu.agh.iosr.pa.rest.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement(name = "patientInfo")
@XmlAccessorType(XmlAccessType.NONE)
public class PatientInfoVO {

	@XmlElementRef
	private PatientVO patient;

	@XmlElementRef
	private List<ActivityVO> activities;

	@XmlElementRef
	private List<AlertVO> alerts;

	@XmlElementRef
	private List<CriticalValueVO> criticalValues;

	@XmlElementRef
	private List<DietVO> diets;

	@XmlElementRef
	private List<ExaminationVO> examinations;

	@XmlElementRef
	private List<MedicamentPatientVO> medicaments;

	@XmlElementRef
	private List<VisitVO> visits;

	public PatientVO getPatient() {
		return patient;
	}

	public void setPatient(PatientVO patient) {
		this.patient = patient;
	}

	public List<ActivityVO> getActivities() {
		return activities;
	}

	public void setActivities(List<ActivityVO> activities) {
		this.activities = activities;
	}

	public List<AlertVO> getAlerts() {
		return alerts;
	}

	public void setAlerts(List<AlertVO> alerts) {
		this.alerts = alerts;
	}

	public List<CriticalValueVO> getCriticalValues() {
		return criticalValues;
	}

	public void setCriticalValues(List<CriticalValueVO> criticalValues) {
		this.criticalValues = criticalValues;
	}

	public List<DietVO> getDiets() {
		return diets;
	}

	public void setDiets(List<DietVO> diets) {
		this.diets = diets;
	}

	public List<ExaminationVO> getExaminations() {
		return examinations;
	}

	public void setExaminations(List<ExaminationVO> examinations) {
		this.examinations = examinations;
	}

	public List<MedicamentPatientVO> getMedicaments() {
		return medicaments;
	}

	public void setMedicaments(List<MedicamentPatientVO> medicaments) {
		this.medicaments = medicaments;
	}

	public List<VisitVO> getVisits() {
		return visits;
	}

	public void setVisits(List<VisitVO> visits) {
		this.visits = visits;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
