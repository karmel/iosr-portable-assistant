package pl.edu.agh.iosr.pa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

@Entity
@Table(name = "IOSR_EX")
public class Examination {

	@Id
	@Column(name = "EX_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	// wiele badan do jednego pacjenta
	@JoinColumn(name = "EX_PAT_ID", nullable = false)
	private Patient patient;

	@Column(name = "EX_DATE", nullable = false)
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime date;

	@Column(name = "EX_VAL", nullable = false)
	private Integer value;

	@ManyToOne(fetch = FetchType.EAGER)
	// wiele badan do jednego typu
	@JoinColumn(name = "EX_EXT_ID")
	private ExaminationType examinationType;

	@ManyToOne(fetch = FetchType.EAGER)
	// wiele badan podczas wizyty
	@JoinColumn(name = "EX_VS_ID", nullable = true)
	private Visit visit;

	public Examination() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public ExaminationType getExaminationType() {
		return examinationType;
	}

	public void setExaminationType(ExaminationType examinationType) {
		this.examinationType = examinationType;
	}

	public Visit getVisit() {
		return visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

}
