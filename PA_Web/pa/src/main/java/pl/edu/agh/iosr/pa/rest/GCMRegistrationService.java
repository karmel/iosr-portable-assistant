package pl.edu.agh.iosr.pa.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.Validate;

@Path("/gcm")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.TEXT_PLAIN })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.TEXT_PLAIN })
public class GCMRegistrationService {

	private String GCM_REGISTRATION_ID = "";

	@POST
	@Path("/{id}")
	public Response setRegistrationID(@PathParam("id") String id) {
		Validate.notBlank(id);
		setGCM_REGISTRATION_ID(id);
		return Response.ok().build();
	}

	@GET
	public String getRegistrationID() {
		return GCM_REGISTRATION_ID;
	}

	public String getGCM_REGISTRATION_ID() {
		return GCM_REGISTRATION_ID;
	}

	public void setGCM_REGISTRATION_ID(String gCM_REGISTRATION_ID) {
		GCM_REGISTRATION_ID = gCM_REGISTRATION_ID;
	}
}
