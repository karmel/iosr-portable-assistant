package pl.edu.agh.iosr.pa.util.db;

public interface DBInitializeManager {

	// initialize DB 
	void init();
}
