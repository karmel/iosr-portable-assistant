package pl.edu.agh.iosr.pa.manager;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import pl.edu.agh.iosr.pa.dao.ActivityTypeDao;
import pl.edu.agh.iosr.pa.dao.ExaminationTypeDao;
import pl.edu.agh.iosr.pa.model.ActivityType;
import pl.edu.agh.iosr.pa.model.ExaminationType;

public class HelperManager {
	private ExaminationTypeDao examinationTypeDao;
	private ActivityTypeDao activityTypeDao;

	@Transactional
	public List<ExaminationType> getExaminationTypes() {
		return examinationTypeDao.findAll(ExaminationType.class);
	}

	@Transactional
	public List<ActivityType> getActivityTypes() {
		return activityTypeDao.findAll(ActivityType.class);
	}

	@Transactional
	public ActivityType findActivityTypeById(long id) {
		return activityTypeDao.findById(ActivityType.class, id);
	}

	@Transactional
	public ExaminationType findExaminationTypeById(long id) {
		return examinationTypeDao.findById(ExaminationType.class, id);
	}

	public void setExaminationTypeDao(ExaminationTypeDao examinationTypeDao) {
		this.examinationTypeDao = examinationTypeDao;
	}

	public void setActivityTypeDao(ActivityTypeDao arg0) {
		this.activityTypeDao = arg0;
	}
}
