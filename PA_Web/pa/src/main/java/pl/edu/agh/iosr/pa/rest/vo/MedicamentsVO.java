package pl.edu.agh.iosr.pa.rest.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "medicaments")
@XmlAccessorType(XmlAccessType.NONE)
public class MedicamentsVO {

	@XmlElementRef
	private List<MedicamentVO> medicaments;

	public List<MedicamentVO> getMedicaments() {
		return medicaments;
	}

	public void setMedicaments(List<MedicamentVO> medicaments) {
		this.medicaments = medicaments;
	}
}
