package pl.edu.agh.iosr.pa.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import pl.edu.agh.iosr.pa.model.Patient;

public class PatientDao extends BaseDao<Patient>{

	@SuppressWarnings("unchecked")
	public List<Patient> searchPatient(PatientSearchCriteria patientSearchCriteria){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Patient.class, "pat");
		
		if(patientSearchCriteria.getAddress()!=null)
			criteria.add(Restrictions.eq("pat.address", patientSearchCriteria.getAddress()));
		if(patientSearchCriteria.getFirstName()!=null)
			criteria.add(Restrictions.eq("pat.firstName", patientSearchCriteria.getFirstName()));
		if(patientSearchCriteria.getLastName()!=null)
			criteria.add(Restrictions.eq("pat.lastName", patientSearchCriteria.getLastName()));
		if(patientSearchCriteria.getLogin()!=null)
			criteria.add(Restrictions.eq("pat.login", patientSearchCriteria.getLogin()));
		
		return criteria.list();
	}
	
	public Patient login(String login, String password){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Patient.class);
		
		criteria.add(Restrictions.eq("login", login));
		criteria.add(Restrictions.eq("password", password));
		
		return (Patient) criteria.uniqueResult();
	}

	public Patient findByLogin(String login) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Patient.class);
		criteria.add(Restrictions.eq("login", login));
		
		return (Patient) criteria.uniqueResult();
	}
	
}
