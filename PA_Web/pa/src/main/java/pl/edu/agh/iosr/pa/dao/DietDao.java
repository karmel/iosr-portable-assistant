package pl.edu.agh.iosr.pa.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.edu.agh.iosr.pa.model.Diet;

public class DietDao extends BaseDao<Diet> {
	@SuppressWarnings("unchecked")
	public List<Diet> getDietsForPatientId(long patientId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Diet.class, "diet");
		criteria.createAlias("diet.patient", "dietPatient");
		criteria.add(Restrictions.eq("dietPatient.id", patientId));
		criteria.addOrder(Order.asc("diet.startDate"));
		return criteria.list();
	}
}
