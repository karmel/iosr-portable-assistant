package pl.edu.agh.iosr.pa.rest.vo.converters;

import pl.edu.agh.iosr.pa.model.Examination;
import pl.edu.agh.iosr.pa.rest.vo.ExaminationVO;

public class ExaminationConverter extends AbstractConverter<Examination, ExaminationVO> {

	public ExaminationConverter() {
		super(Examination.class, ExaminationVO.class);
	}

	public ExaminationConverter(Class<Examination> tclazz, Class<ExaminationVO> voclazz) {
		super(tclazz, voclazz);
	}

	@Override
	public Examination convertFromVO(ExaminationVO vo, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		Examination e = super.convertFromVO(vo, simpleConverter);
		e.setExaminationType(new ExaminationTypeConverter().convertFromVO(vo.getExaminationType(), simpleConverter));
		if (e.getVisit() != null) {
			e.setVisit(new VisitConverter().convertFromVO(vo.getVisit(), simpleConverter));
		}
		if (!simpleConverter) {
			e.setPatient(new PatientConverter().convertFromVO(vo.getPatient(), simpleConverter));
		}
		return e;
	}

	@Override
	public ExaminationVO convertToVO(Examination t, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		ExaminationVO vo = super.convertToVO(t, simpleConverter);
		vo.setExaminationType(new ExaminationTypeConverter().convertToVO(t.getExaminationType(), simpleConverter));
		if (t.getVisit() != null) {
			vo.setVisit(new VisitConverter().convertToVO(t.getVisit(), simpleConverter));
		}
		if (!simpleConverter) {
			vo.setPatient(new PatientConverter().convertToVO(t.getPatient(), simpleConverter));
		}
		return vo;
	}
}
