package pl.edu.agh.iosr.pa.rest.vo.converters;

import pl.edu.agh.iosr.pa.model.Medicament;
import pl.edu.agh.iosr.pa.rest.vo.MedicamentVO;

public class MedicamentConverter extends AbstractConverter<Medicament, MedicamentVO> {

	public MedicamentConverter() {
		super(Medicament.class, MedicamentVO.class);
	}

	public MedicamentConverter(Class<Medicament> tclazz, Class<MedicamentVO> voclazz) {
		super(tclazz, voclazz);
	}

}
