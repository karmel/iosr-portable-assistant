package pl.edu.agh.iosr.pa.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.edu.agh.iosr.pa.model.Activity;

public class ActivityDao extends BaseDao<Activity> {
	@SuppressWarnings("unchecked")
	public List<Activity> getActivitiesForPatientId(long patientId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Activity.class, "act");
		criteria.createAlias("act.patient", "actPatient");
		criteria.add(Restrictions.eq("actPatient.id", patientId));
		criteria.addOrder(Order.asc("act.startDate"));

		return criteria.list();
	}

}
