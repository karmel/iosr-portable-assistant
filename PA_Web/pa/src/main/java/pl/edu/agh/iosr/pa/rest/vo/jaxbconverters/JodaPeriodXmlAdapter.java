package pl.edu.agh.iosr.pa.rest.vo.jaxbconverters;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.Period;
import org.joda.time.format.PeriodFormat;

public class JodaPeriodXmlAdapter extends XmlAdapter<String, Period> {

	@Override
	public String marshal(Period arg0) throws Exception {
		return PeriodFormat.getDefault().print(arg0);
	}

	@Override
	public Period unmarshal(String arg0) throws Exception {
		return PeriodFormat.getDefault().parsePeriod(arg0);
	}

}
