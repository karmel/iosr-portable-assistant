package pl.edu.agh.iosr.pa.rest.vo.converters;

import pl.edu.agh.iosr.pa.model.Patient;
import pl.edu.agh.iosr.pa.rest.vo.PatientVO;

public class PatientConverter extends AbstractConverter<Patient, PatientVO> {

	public PatientConverter() {
		super(Patient.class, PatientVO.class);
	}

	public PatientConverter(Class<Patient> tclazz, Class<PatientVO> voclazz) {
		super(tclazz, voclazz);
	}

	@Override
	public Patient convertFromVO(PatientVO vo, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		Patient p = super.convertFromVO(vo, simpleConverter);
		if (!simpleConverter) {
			p.setDoctor(new DoctorConverter().convertFromVO(vo.getDoctor(), simpleConverter));
		}
		return p;
	}

	@Override
	public PatientVO convertToVO(Patient t, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		PatientVO pv = super.convertToVO(t, simpleConverter);
		if (!simpleConverter) {
			pv.setDoctor(new DoctorConverter().convertToVO(t.getDoctor(), simpleConverter));
		}
		pv.setPassword("PROTECTED");
		return pv;
	}
}
