package pl.edu.agh.iosr.pa.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import pl.edu.agh.iosr.pa.model.MedicamentPatient;

public class MedicamentPatientDao extends BaseDao<MedicamentPatient>{
	@SuppressWarnings("unchecked")
	public List<MedicamentPatient> getMedicamentsForPatientId(long patientId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(MedicamentPatient.class, "mp");
		criteria.createAlias("mp.patient", "mpPatient");
		criteria.add(Restrictions.eq("mpPatient.id", patientId));
		return criteria.list();
	}
}
