package pl.edu.agh.iosr.pa.rest.vo.converters;

import pl.edu.agh.iosr.pa.model.MedicamentPatient;
import pl.edu.agh.iosr.pa.rest.vo.MedicamentPatientVO;

public class MedicamentPatientConverter extends AbstractConverter<MedicamentPatient, MedicamentPatientVO> {

	public MedicamentPatientConverter() {
		super(MedicamentPatient.class, MedicamentPatientVO.class);
	}

	public MedicamentPatientConverter(Class<MedicamentPatient> tclazz, Class<MedicamentPatientVO> voclazz) {
		super(tclazz, voclazz);
	}

	@Override
	public MedicamentPatient convertFromVO(MedicamentPatientVO vo, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		MedicamentPatient mp = super.convertFromVO(vo, simpleConverter);
		mp.setMedicament(new MedicamentConverter().convertFromVO(vo.getMedicament(), simpleConverter));
		if (!simpleConverter) {
			mp.setPatient(new PatientConverter().convertFromVO(vo.getPatient(), simpleConverter));
		}
		return mp;
	}

	@Override
	public MedicamentPatientVO convertToVO(MedicamentPatient t, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		MedicamentPatientVO mpv = super.convertToVO(t, simpleConverter);
		mpv.setMedicament(new MedicamentConverter().convertToVO(t.getMedicament(), simpleConverter));
		if (!simpleConverter) {
			mpv.setPatient(new PatientConverter().convertToVO(t.getPatient(), simpleConverter));
		}
		return mpv;
	}
}
