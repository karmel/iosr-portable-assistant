package pl.edu.agh.iosr.pa.ws.controllers;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import pl.edu.agh.iosr.pa.ws.model.Greeting;
import pl.edu.agh.iosr.pa.ws.model.HelloMessage;

@Controller
public class AlertsController {

	private static Logger logger = Logger.getLogger(AlertsController.class);

	@PostConstruct
	public void init() {
		logger.debug("After AlertsController creation!");
	}

	@MessageMapping({ "/connection", "/connection/**" })
	@SendTo("/alerts/greetings")
	public Greeting connect(HelloMessage greet) {
		return new Greeting("Hello, " + greet.getName()
				+ ", you are now connected!");
	}
}
