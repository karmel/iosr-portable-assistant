package pl.edu.agh.iosr.pa.rest.vo.converters;

import pl.edu.agh.iosr.pa.model.Doctor;
import pl.edu.agh.iosr.pa.rest.vo.DoctorVO;

public class DoctorConverter extends AbstractConverter<Doctor, DoctorVO> {

	public DoctorConverter() {
		super(Doctor.class, DoctorVO.class);
	}

	public DoctorConverter(Class<Doctor> tclazz, Class<DoctorVO> voclazz) {
		super(tclazz, voclazz);
	}

	@Override
	public Doctor convertFromVO(DoctorVO vo, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		Doctor doc = super.convertFromVO(vo, simpleConverter);
		if (!simpleConverter) {
			doc.setSpecialization(new SpecializationConverter().convertFromVO(vo.getSpecialization(), simpleConverter));
		}
		return doc;
	}

	@Override
	public DoctorVO convertToVO(Doctor t, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		DoctorVO vo = super.convertToVO(t, simpleConverter);
		if (!simpleConverter) {
			vo.setSpecialization(new SpecializationConverter().convertToVO(t.getSpecialization(), simpleConverter));
		}
		vo.setPassword("PROTECTED");
		return vo;
	}
}
