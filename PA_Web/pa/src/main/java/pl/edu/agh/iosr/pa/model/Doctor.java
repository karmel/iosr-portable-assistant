package pl.edu.agh.iosr.pa.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="IOSR_DR")
public class Doctor {

	@Id
	@Column(name="DR_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="DR_FIST_NAME", nullable = false)
	private String firstName;
	
	@Column(name="DR_LAST_NAME", nullable = false)
	private String lastName;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL) //wiele doktorow do jednej specjalizacji?
	@JoinColumn(name="DR_SPC_ID")
	private Specialization specialization;
	
	@Column(name="DR_ADDR", nullable = false)
	private String address;
	
	@Column(name="DR_PHONE", nullable = false)
	private String phone;
	
	@Column(name="DR_LOGIN", nullable = false, unique=true)
	private String login;
	
	@Column(name="DR_PASS", nullable = false)
	private String password;
	
	public Doctor(){}
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public Specialization getSpecialization() {
		return specialization;
	}

	public void setSpecialization(Specialization specialization) {
		this.specialization = specialization;
	}


	@Override
	public String toString() {
		return "Doctor [id=" + id + ", firstName=" + firstName + ", lastName="
				+ lastName + ", doctorSpecialization=" + specialization
				+ ", address=" + address + ", phone=" + phone + "]";
	}
	
	
}
