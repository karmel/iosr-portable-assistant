package pl.edu.agh.iosr.pa.security;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import pl.edu.agh.iosr.pa.manager.UserManager;
import pl.edu.agh.iosr.pa.model.Doctor;
import pl.edu.agh.iosr.pa.model.Patient;

/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

	private final Logger log = LoggerFactory.getLogger(UserDetailsService.class);

	@Autowired
	private UserManager userManager;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(final String login) {
		log.debug("Authenticating {}", login);
		String lowercaseLogin = login.toLowerCase();

		Object userFromDatabase = getUserManager().getDoctorByLogin(login);
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		String password = null;
		if (userFromDatabase == null) {
			userFromDatabase = getUserManager().getPatientByLogin(login);
			if (userFromDatabase == null) {
				throw new UsernameNotFoundException("User " + lowercaseLogin + " was not found in the database");
			} else {
				GrantedAuthority ga = new SimpleGrantedAuthority(AuthoritiesConstants.USER);
				grantedAuthorities.add(ga);
				password = ((Patient) userFromDatabase).getPassword();
			}
		} else {
			GrantedAuthority ga = new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN);
			grantedAuthorities.add(ga);
			password = ((Doctor) userFromDatabase).getPassword();
		}

		return new org.springframework.security.core.userdetails.User(lowercaseLogin, password, grantedAuthorities);
	}

	public UserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
}
