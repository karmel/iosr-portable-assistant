package pl.edu.agh.iosr.pa.rest.vo.converters;

import pl.edu.agh.iosr.pa.model.Specialization;
import pl.edu.agh.iosr.pa.rest.vo.SpecializationVO;

public class SpecializationConverter extends AbstractConverter<Specialization, SpecializationVO> {

	public SpecializationConverter() {
		super(Specialization.class, SpecializationVO.class);
	}

	public SpecializationConverter(Class<Specialization> tclazz, Class<SpecializationVO> voclazz) {
		super(tclazz, voclazz);
	}

}
