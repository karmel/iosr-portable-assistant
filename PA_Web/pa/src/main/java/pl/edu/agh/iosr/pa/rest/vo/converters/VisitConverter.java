package pl.edu.agh.iosr.pa.rest.vo.converters;

import pl.edu.agh.iosr.pa.model.Visit;
import pl.edu.agh.iosr.pa.rest.vo.VisitVO;

public class VisitConverter extends AbstractConverter<Visit, VisitVO> {

	public VisitConverter() {
		super(Visit.class, VisitVO.class);
	}

	public VisitConverter(Class<Visit> tclazz, Class<VisitVO> voclazz) {
		super(tclazz, voclazz);
	}

	@Override
	public Visit convertFromVO(VisitVO vo, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		Visit v = super.convertFromVO(vo, simpleConverter);
		v.setDoctor(new DoctorConverter().convertFromVO(vo.getDoctor(), false));
		if (!simpleConverter) {
			v.setPatient(new PatientConverter().convertFromVO(vo.getPatient(), simpleConverter));
		}
		return v;
	}

	@Override
	public VisitVO convertToVO(Visit t, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		VisitVO vo = super.convertToVO(t, simpleConverter);
		vo.setDoctor(new DoctorConverter().convertToVO(t.getDoctor(), false));
		if (!simpleConverter) {
			vo.setPatient(new PatientConverter().convertToVO(t.getPatient(), simpleConverter));
		}
		return vo;
	}
}
