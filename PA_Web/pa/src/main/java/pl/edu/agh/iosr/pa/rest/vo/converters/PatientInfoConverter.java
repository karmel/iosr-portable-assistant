package pl.edu.agh.iosr.pa.rest.vo.converters;

import pl.edu.agh.iosr.pa.model.PatientInfo;
import pl.edu.agh.iosr.pa.rest.vo.PatientInfoVO;

public class PatientInfoConverter extends AbstractConverter<PatientInfo, PatientInfoVO> {

	public PatientInfoConverter() {
		super(PatientInfo.class, PatientInfoVO.class);
	}

	public PatientInfoConverter(Class<PatientInfo> tclazz, Class<PatientInfoVO> voclazz) {
		super(tclazz, voclazz);
	}

	@Override
	public PatientInfo convertFromVO(PatientInfoVO vo, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		PatientInfo pi = super.convertFromVO(vo, simpleConverter);
		pi.setActivities(new ActivityConverter().convertListFromVOs(vo.getActivities(), true));
		pi.setAlerts(new AlertConverter().convertListFromVOs(vo.getAlerts(), true));
		pi.setCriticalValues(new CriticalValueConverter().convertListFromVOs(vo.getCriticalValues(), true));
		pi.setDiets(new DietConverter().convertListFromVOs(vo.getDiets(), true));
		pi.setExaminations(new ExaminationConverter().convertListFromVOs(vo.getExaminations(), true));
		pi.setMedicaments(new MedicamentPatientConverter().convertListFromVOs(vo.getMedicaments(), true));
		pi.setPatient(new PatientConverter().convertFromVO(vo.getPatient(), false));
		pi.setVisits(new VisitConverter().convertListFromVOs(vo.getVisits(), true));
		return pi;
	}

	@Override
	public PatientInfoVO convertToVO(PatientInfo t, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		PatientInfoVO pi = super.convertToVO(t, simpleConverter);
		pi.setActivities(new ActivityConverter().convertListToVOs(t.getActivities(), true));
		pi.setAlerts(new AlertConverter().convertListToVOs(t.getAlerts(), true));
		pi.setCriticalValues(new CriticalValueConverter().convertListToVOs(t.getCriticalValues(), true));
		pi.setDiets(new DietConverter().convertListToVOs(t.getDiets(), true));
		pi.setExaminations(new ExaminationConverter().convertListToVOs(t.getExaminations(), true));
		pi.setMedicaments(new MedicamentPatientConverter().convertListToVOs(t.getMedicaments(), true));
		pi.setPatient(new PatientConverter().convertToVO(t.getPatient(), false));
		pi.setVisits(new VisitConverter().convertListToVOs(t.getVisits(), true));
		return pi;
	}
}
