package pl.edu.agh.iosr.pa.model;

import java.util.List;

public class PatientInfo {
	
	private Patient patient;
	private List<Visit> visits;
	private List<Examination> examinations;
	private List<MedicamentPatient> medicaments;
	private List<Diet> diets;
	private List<Activity> activities;
	private List<Alert> alerts;
	private List<CriticalValue> criticalValues;
	
	
	public PatientInfo() {
	}


	public Patient getPatient() {
		return patient;
	}


	public void setPatient(Patient patient) {
		this.patient = patient;
	}


	public List<Visit> getVisits() {
		return visits;
	}


	public void setVisits(List<Visit> visits) {
		this.visits = visits;
	}


	public List<Examination> getExaminations() {
		return examinations;
	}


	public void setExaminations(List<Examination> examinations) {
		this.examinations = examinations;
	}


	public List<MedicamentPatient> getMedicaments() {
		return medicaments;
	}


	public void setMedicaments(List<MedicamentPatient> medicaments) {
		this.medicaments = medicaments;
	}


	public List<Diet> getDiets() {
		return diets;
	}


	public void setDiets(List<Diet> diets) {
		this.diets = diets;
	}


	public List<Activity> getActivities() {
		return activities;
	}


	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}


	public List<Alert> getAlerts() {
		return alerts;
	}


	public void setAlerts(List<Alert> alerts) {
		this.alerts = alerts;
	}


	public List<CriticalValue> getCriticalValues() {
		return criticalValues;
	}


	public void setCriticalValues(List<CriticalValue> criticalValues) {
		this.criticalValues = criticalValues;
	}
	

}
