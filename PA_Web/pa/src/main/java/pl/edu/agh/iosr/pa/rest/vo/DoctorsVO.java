package pl.edu.agh.iosr.pa.rest.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement(name = "doctors")
@XmlAccessorType(XmlAccessType.NONE)
public class DoctorsVO {

	@XmlElementRef
	private List<DoctorVO> doctors;

	public List<DoctorVO> getDoctors() {
		return doctors;
	}

	public void setDoctors(List<DoctorVO> doctors) {
		this.doctors = doctors;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
