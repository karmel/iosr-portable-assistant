package pl.edu.agh.iosr.pa.manager;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import pl.edu.agh.iosr.pa.dao.ActivityDao;
import pl.edu.agh.iosr.pa.dao.AlertDao;
import pl.edu.agh.iosr.pa.dao.CriticalValueDao;
import pl.edu.agh.iosr.pa.dao.DietDao;
import pl.edu.agh.iosr.pa.dao.DoctorDao;
import pl.edu.agh.iosr.pa.dao.ExaminationDao;
import pl.edu.agh.iosr.pa.dao.MedicamentPatientDao;
import pl.edu.agh.iosr.pa.dao.PatientDao;
import pl.edu.agh.iosr.pa.dao.PatientSearchCriteria;
import pl.edu.agh.iosr.pa.dao.VisitDao;
import pl.edu.agh.iosr.pa.exception.LoginException;
import pl.edu.agh.iosr.pa.model.Doctor;
import pl.edu.agh.iosr.pa.model.Patient;
import pl.edu.agh.iosr.pa.model.PatientInfo;

public class UserManager {

	private DoctorDao doctorDao;
	private VisitDao visitDao;
	private PatientDao patientDao;
	private ActivityDao activityDao;
	private AlertDao alertDao;
	private CriticalValueDao criticalValueDao;
	private DietDao dietDao;
	private ExaminationDao examinationDao;
	private MedicamentPatientDao medicamentPatientDao;

	@Transactional(readOnly = true)
	public PatientInfo getPatientInfoByPatientId(Long id) {
		PatientInfo patientInfo = new PatientInfo();
		patientInfo.setPatient(patientDao.findById(Patient.class, id));
		patientInfo.setActivities(activityDao.getActivitiesForPatientId(id));
		patientInfo.setAlerts(alertDao.getAlertsForPatientId(id));
		patientInfo.setCriticalValues(criticalValueDao.getCriticalValuesForPatientId(id));
		patientInfo.setDiets(dietDao.getDietsForPatientId(id));
		patientInfo.setExaminations(examinationDao.getExaminationsForPatientId(id));
		patientInfo.setMedicaments(medicamentPatientDao.getMedicamentsForPatientId(id));
		patientInfo.setVisits(visitDao.getVisitsForPatientId(id));

		return patientInfo;
	}

	@Transactional
	public Doctor getDoctorByLogin(String login) {
		return doctorDao.findByLogin(login);
	}

	@Transactional
	public Patient getPatientByLogin(String login) {
		return patientDao.findByLogin(login);
	}

	@Transactional
	public Doctor loginDoctor(String login, String password) throws LoginException {
		String encryptedPassword = null;
		try {
			encryptedPassword = encryptPassword(password);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return doctorDao.login(login, encryptedPassword);
	}

	@Transactional
	public Patient loginPatient(String login, String password) throws LoginException {
		String encryptedPassword = null;
		try {
			encryptedPassword = encryptPassword(password);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return patientDao.login(login, encryptedPassword);
	}

	@Transactional
	public Serializable addPatient(Patient patient) {
		String encryptedPassword = null;
		try {
			encryptedPassword = encryptPassword(patient.getPassword());
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		patient.setPassword(encryptedPassword);
		return patientDao.save(patient);
	}

	@Transactional
	public List<Patient> searchPatients(PatientSearchCriteria patientSearchCriteria) {
		return patientDao.searchPatient(patientSearchCriteria);
	}

	@Transactional(readOnly = true)
	public List<Doctor> getAllDoctors() {
		return doctorDao.findAll(Doctor.class);
	}

	@Transactional(readOnly = true)
	public List<Patient> getAllPatients() {
		return patientDao.findAll(Patient.class);
	}

	@Transactional(readOnly = true)
	public Doctor getDoctorById(long id) {
		return doctorDao.findById(Doctor.class, id);
	}

	@Transactional(readOnly = true)
	public Patient getPatientById(long id) {
		return patientDao.findById(Patient.class, id);
	}

	public static String encryptPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(password.getBytes("UTF-8"));
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < hash.length; i++) {
			sb.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
		// return new String(hash);
	}

	public void setPatientDao(PatientDao patientDao) {
		this.patientDao = patientDao;
	}

	public void setActivityDao(ActivityDao activityDao) {
		this.activityDao = activityDao;
	}

	public void setAlertDao(AlertDao alertDao) {
		this.alertDao = alertDao;
	}

	public void setCriticalValueDao(CriticalValueDao criticalValueDao) {
		this.criticalValueDao = criticalValueDao;
	}

	public void setDietDao(DietDao dietDao) {
		this.dietDao = dietDao;
	}

	public void setExaminationDao(ExaminationDao examinationDao) {
		this.examinationDao = examinationDao;
	}

	public void setMedicamentPatientDao(MedicamentPatientDao medicamentDao) {
		this.medicamentPatientDao = medicamentDao;
	}

	public void setDoctorDao(DoctorDao doctorDao) {
		this.doctorDao = doctorDao;
	}

	public void setVisitDao(VisitDao visitDao) {
		this.visitDao = visitDao;
	}

}
