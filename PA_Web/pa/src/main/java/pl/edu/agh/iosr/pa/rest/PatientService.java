package pl.edu.agh.iosr.pa.rest;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.Validate;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import pl.edu.agh.iosr.pa.manager.UserManager;
import pl.edu.agh.iosr.pa.model.Patient;
import pl.edu.agh.iosr.pa.model.PatientInfo;
import pl.edu.agh.iosr.pa.rest.vo.PatientInfoVO;
import pl.edu.agh.iosr.pa.rest.vo.PatientVO;
import pl.edu.agh.iosr.pa.rest.vo.PatientsVO;
import pl.edu.agh.iosr.pa.rest.vo.converters.PatientConverter;
import pl.edu.agh.iosr.pa.rest.vo.converters.PatientInfoConverter;

@Path("/patient")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.TEXT_PLAIN })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.TEXT_PLAIN })
public class PatientService {

	private Logger logger = Logger.getLogger(PatientService.class);

	private UserManager userManager;

	@PostConstruct
	public void init() {
		if (userManager == null) {
			userManager = (UserManager) SpringContextLoaderListener.getCurrentWebApplicationContext().getBean("userManager");
		}
		logger.debug("PatientService initialized");
	}

	@Path("/test")
	@GET
	public PatientVO testMethod() {
		logger.debug("Got request for PatientService");
		PatientVO pv = new PatientVO();
		pv.setAddress("siakis adres");
		pv.setFirstName("Aral");
		pv.setLastName("No tyz Aral");
		pv.setId((long) 1);
		pv.setLogin("aral");
		pv.setPassword("aral");
		return pv;
	}

	@GET
	@Transactional
	public PatientsVO getAllPatients() throws InstantiationException, IllegalAccessException {
		List<PatientVO> allPatients = new PatientConverter().convertListToVOs(getUserManager().getAllPatients(), false);
		PatientsVO ap = new PatientsVO();
		ap.setPatients(allPatients);
		return ap;
	}

	@Path("/{id}")
	@GET
	@Transactional
	public PatientInfoVO getPatientInfoById(@PathParam(value = "id") long id) throws InstantiationException, IllegalAccessException {
		Validate.notNull(id);
		Patient pat = getUserManager().getPatientById(id);
		Validate.notNull(pat);
		PatientInfo pInfo = getUserManager().getPatientInfoByPatientId(id);
		Validate.notNull(pInfo);
		return new PatientInfoConverter().convertToVO(pInfo, false);
	}

	@Path("/info/{login}")
	@GET
	@Transactional
	public PatientInfoVO getPatientInfoByLogin(@PathParam(value = "login") String login) throws InstantiationException, IllegalAccessException {
		Validate.notBlank(login);
		Patient p = userManager.getPatientByLogin(login);
		Validate.notNull(p);
		Validate.notNull(p.getId());
		PatientInfo pInfo = getUserManager().getPatientInfoByPatientId(p.getId());
		Validate.notNull(pInfo);
		return new PatientInfoConverter().convertToVO(pInfo, false);
	}

	@Path("/{login}")
	@GET
	@Transactional
	public PatientVO getPatientByLogin(@PathParam(value = "login") String login) throws InstantiationException, IllegalAccessException {
		Validate.notBlank(login);
		Patient p = userManager.getPatientByLogin(login);
		Validate.notNull(p);
		return new PatientConverter().convertToVO(p, false);
	}

	public UserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

}
