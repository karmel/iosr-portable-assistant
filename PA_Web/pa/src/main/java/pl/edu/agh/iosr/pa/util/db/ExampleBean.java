package pl.edu.agh.iosr.pa.util.db;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

public class ExampleBean implements ApplicationListener<ContextRefreshedEvent>  {
	private Logger logger = Logger.getLogger(ExampleBean.class);
	private boolean firstRun;
	private DBInitializeManager exampleManager;
	
	public void init() {
		if (firstRun) {
			logger.info("firstRun==true... proceed with initialization");
			exampleManager.init();
		}
	}

//	private void exampleUsage() {
////		exampleManager.exampleSave();
//		
//		List<Doctor> list = exampleManager.exampleFindsBySpecializationName("docSpecs");
//		logger.info("+++++++++++++++++++++++++");
//		logger.info("+++++++++++++++++++++++++");
//		logger.info("+++++++++++++++++++++++++");
//		logger.info("Found"  + list.size() + " doctors");
//		for (Doctor d : list) {
//			logger.info("Doctor found:" + d);
//		}
//	}
	
	public void setFirstRun(boolean firstRun) {
		this.firstRun = firstRun;
	}


	public void setExampleManager(DBInitializeManager exampleManager) {
		this.exampleManager = exampleManager;
	}


	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
//		exampleUsage();
	}
	
	
	
}
