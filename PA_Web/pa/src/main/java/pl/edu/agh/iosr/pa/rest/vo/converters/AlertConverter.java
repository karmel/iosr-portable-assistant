package pl.edu.agh.iosr.pa.rest.vo.converters;

import pl.edu.agh.iosr.pa.model.Alert;
import pl.edu.agh.iosr.pa.rest.vo.AlertVO;

public class AlertConverter extends AbstractConverter<Alert, AlertVO> {

	public AlertConverter() {
		super(Alert.class, AlertVO.class);
	}

	public AlertConverter(Class<Alert> tclazz, Class<AlertVO> voclazz) {
		super(tclazz, voclazz);
	}

	@Override
	public Alert convertFromVO(AlertVO vo, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		Alert a = super.convertFromVO(vo, simpleConverter);
		a.setDoctor(new DoctorConverter().convertFromVO(vo.getDoctor(), simpleConverter));
		if (!simpleConverter) {
			a.setPatient(new PatientConverter().convertFromVO(vo.getPatient(), simpleConverter));
		}
		return a;
	}

	@Override
	public AlertVO convertToVO(Alert t, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		AlertVO a = super.convertToVO(t, simpleConverter);
		a.setDoctor(new DoctorConverter().convertToVO(t.getDoctor(), simpleConverter));
		if (!simpleConverter) {
			a.setPatient(new PatientConverter().convertToVO(t.getPatient(), simpleConverter));
		}
		return a;
	}
}
