package pl.edu.agh.iosr.pa.manager;

import java.io.Serializable;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.transaction.annotation.Transactional;

import pl.edu.agh.iosr.pa.dao.ActivityDao;
import pl.edu.agh.iosr.pa.dao.AlertDao;
import pl.edu.agh.iosr.pa.dao.CriticalValueDao;
import pl.edu.agh.iosr.pa.dao.DietDao;
import pl.edu.agh.iosr.pa.dao.ExaminationDao;
import pl.edu.agh.iosr.pa.dao.MedicamentDao;
import pl.edu.agh.iosr.pa.dao.MedicamentPatientDao;
import pl.edu.agh.iosr.pa.dao.VisitDao;
import pl.edu.agh.iosr.pa.model.Activity;
import pl.edu.agh.iosr.pa.model.Alert;
import pl.edu.agh.iosr.pa.model.CriticalValue;
import pl.edu.agh.iosr.pa.model.Diet;
import pl.edu.agh.iosr.pa.model.Doctor;
import pl.edu.agh.iosr.pa.model.Examination;
import pl.edu.agh.iosr.pa.model.Medicament;
import pl.edu.agh.iosr.pa.model.MedicamentPatient;
import pl.edu.agh.iosr.pa.model.Patient;
import pl.edu.agh.iosr.pa.model.Visit;

public class TreatmentManager {
	private VisitDao visitDao;
	private ActivityDao activityDao;
	private MedicamentDao medicamentDao;
	private MedicamentPatientDao medicamentPatientDao;
	private DietDao dietDao;
	private ExaminationDao examinationDao;
	private CriticalValueDao criticalValueDao;
	private AlertDao alertDao;

	@Transactional
	public Serializable addVisit(LocalDateTime date, Doctor doctor, Patient patient) {
		Visit visit = new Visit();
		visit.setDate(date);
		visit.setDoctor(doctor);
		visit.setPatient(patient);

		return visitDao.save(visit);
	}

	@Transactional
	public Serializable addVisit(Visit visit) {
		return visitDao.save(visit);
	}

	@Transactional
	public Serializable addActivity(Activity activity) {
		return activityDao.save(activity);
	}

	@Transactional
	public void addActivities(List<Activity> activities) {
		for (Activity activity : activities) {
			activityDao.save(activity);
		}
	}

	@Transactional
	public Serializable addMedicament(Medicament medicament) {
		return medicamentDao.save(medicament);
	}

	@Transactional
	public void addMedicaments(List<Medicament> medicaments) {
		for (Medicament medicament : medicaments) {
			medicamentDao.save(medicament);
		}
	}

	@Transactional
	public Serializable addMedicamentPatient(MedicamentPatient medicamentPatient) {
		return getMedicamentPatientDao().save(medicamentPatient);
	}

	@Transactional
	public void addMedicamentPatients(List<MedicamentPatient> medicamentPatients) {
		for (MedicamentPatient mp : medicamentPatients) {
			getMedicamentPatientDao().save(mp);
		}
	}

	@Transactional
	public Serializable addDiet(Diet diet) {
		return dietDao.save(diet);
	}

	@Transactional
	public void addDiets(List<Diet> diets) {
		for (Diet diet : diets) {
			dietDao.save(diet);
		}
	}

	@Transactional
	public Serializable addExamination(Examination examination) {
		List<CriticalValue> cvs = criticalValueDao.getCriticalValuesForPatientId(examination.getPatient().getId());
		for (CriticalValue cv : cvs) {
			if (cv.getExaminationType().getId().equals(examination.getExaminationType().getId())) {
				if (examination.getValue() > cv.getMaxValue()) {
					Alert a = new Alert();
					a.setCause("Examination: " + examination.getExaminationType().getName() + " exceeds critical max value! Max value: " + cv.getMaxValue()
							+ ", current value: " + examination.getValue());
					a.setDoctor(examination.getPatient().getDoctor());
					a.setPatient(examination.getPatient());
					alertDao.save(a);
				} else if (examination.getValue() < cv.getMinValue()) {
					Alert a = new Alert();
					a.setCause("Examination: " + examination.getExaminationType().getName() + " is lower than critical min value! Min value: "
							+ cv.getMinValue() + ", current value: " + examination.getValue());
					a.setDoctor(examination.getPatient().getDoctor());
					a.setPatient(examination.getPatient());
					alertDao.save(a);
				}
				break;
			}
		}
		return examinationDao.save(examination);
	}

	@Transactional
	public List<Medicament> getAllMedicaments() {
		return medicamentDao.findAll(Medicament.class);
	}

	public void setVisitDao(VisitDao visitDao) {
		this.visitDao = visitDao;
	}

	public void setActivityDao(ActivityDao activityDao) {
		this.activityDao = activityDao;
	}

	public void setMedicamentDao(MedicamentDao medicamentDao) {
		this.medicamentDao = medicamentDao;
	}

	public void setDietDao(DietDao dietDao) {
		this.dietDao = dietDao;
	}

	public void setExaminationDao(ExaminationDao examinationDao) {
		this.examinationDao = examinationDao;
	}

	public MedicamentPatientDao getMedicamentPatientDao() {
		return medicamentPatientDao;
	}

	public void setMedicamentPatientDao(MedicamentPatientDao medicamentPatientDao) {
		this.medicamentPatientDao = medicamentPatientDao;
	}

	public CriticalValueDao getCriticalValueDao() {
		return criticalValueDao;
	}

	public void setCriticalValueDao(CriticalValueDao criticalValueDao) {
		this.criticalValueDao = criticalValueDao;
	}

	public AlertDao getAlertDao() {
		return alertDao;
	}

	public void setAlertDao(AlertDao alertDao) {
		this.alertDao = alertDao;
	}

	@Transactional
	public Visit findVisitById(Long id) {
		return visitDao.findById(Visit.class, id);
	}

	@Transactional
	public Medicament findMedicamentById(Long id) {
		return medicamentDao.findById(Medicament.class, id);
	}

}
