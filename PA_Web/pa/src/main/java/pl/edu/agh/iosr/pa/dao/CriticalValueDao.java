package pl.edu.agh.iosr.pa.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import pl.edu.agh.iosr.pa.model.CriticalValue;

public class CriticalValueDao extends BaseDao<CriticalValue> {
	@SuppressWarnings("unchecked")
	public List<CriticalValue> getCriticalValuesForPatientId(long patientId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(CriticalValue.class, "cv");
		criteria.createAlias("cv.patient", "cvPatient");
		criteria.add(Restrictions.eq("cvPatient.id", patientId));
		return criteria.list();
	}
}
