package pl.edu.agh.iosr.pa.rest.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.joda.time.LocalDateTime;

@XmlRootElement(name = "addVisit")
@XmlAccessorType(XmlAccessType.NONE)
public class AddVisitVO {

	@XmlAttribute
	private Long patientId;

	@XmlAttribute
	private Long doctorId;

	@XmlAttribute
	private LocalDateTime date;

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

}
