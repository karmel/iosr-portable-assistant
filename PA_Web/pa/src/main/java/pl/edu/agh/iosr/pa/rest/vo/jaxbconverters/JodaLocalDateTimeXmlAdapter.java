package pl.edu.agh.iosr.pa.rest.vo.jaxbconverters;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.LocalDateTime;

public class JodaLocalDateTimeXmlAdapter extends XmlAdapter<String, LocalDateTime> {

	@Override
	public String marshal(LocalDateTime arg0) throws Exception {
		return arg0.toString();
	}

	@Override
	public LocalDateTime unmarshal(String arg0) throws Exception {
		return LocalDateTime.parse(arg0);
	}

}
