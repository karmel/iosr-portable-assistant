package pl.edu.agh.iosr.pa.rest.vo.converters;

import pl.edu.agh.iosr.pa.model.CriticalValue;
import pl.edu.agh.iosr.pa.rest.vo.CriticalValueVO;

public class CriticalValueConverter extends AbstractConverter<CriticalValue, CriticalValueVO> {

	public CriticalValueConverter() {
		super(CriticalValue.class, CriticalValueVO.class);
	}

	public CriticalValueConverter(Class<CriticalValue> tclazz, Class<CriticalValueVO> voclazz) {
		super(tclazz, voclazz);
	}

	@Override
	public CriticalValue convertFromVO(CriticalValueVO vo, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		CriticalValue cv = super.convertFromVO(vo, simpleConverter);

		cv.setExaminationType(new ExaminationTypeConverter().convertFromVO(vo.getExaminationType(), simpleConverter));
		if (!simpleConverter) {
			cv.setPatient(new PatientConverter().convertFromVO(vo.getPatient(), simpleConverter));
		}
		return cv;
	}

	@Override
	public CriticalValueVO convertToVO(CriticalValue t, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		CriticalValueVO vo = super.convertToVO(t, simpleConverter);
		vo.setExaminationType(new ExaminationTypeConverter().convertToVO(t.getExaminationType(), simpleConverter));
		if (!simpleConverter) {
			vo.setPatient(new PatientConverter().convertToVO(t.getPatient(), simpleConverter));
		}
		return vo;
	}

}
