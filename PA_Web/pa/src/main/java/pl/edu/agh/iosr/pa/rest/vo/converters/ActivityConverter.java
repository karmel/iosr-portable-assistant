package pl.edu.agh.iosr.pa.rest.vo.converters;

import pl.edu.agh.iosr.pa.model.Activity;
import pl.edu.agh.iosr.pa.rest.vo.ActivityVO;

public class ActivityConverter extends AbstractConverter<Activity, ActivityVO> {

	public ActivityConverter() {
		super(Activity.class, ActivityVO.class);
	}

	public ActivityConverter(Class<Activity> tclazz, Class<ActivityVO> voclazz) {
		super(tclazz, voclazz);
	}

	@Override
	public Activity convertFromVO(ActivityVO vo, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		Activity av = super.convertFromVO(vo, simpleConverter);
		av.setActivityType(new ActivityTypeConverter().convertFromVO(vo.getActivityType(), simpleConverter));
		if (!simpleConverter)
			av.setPatient(new PatientConverter().convertFromVO(vo.getPatient(), simpleConverter));
		return av;
	}

	@Override
	public ActivityVO convertToVO(Activity t, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		ActivityVO av = super.convertToVO(t, simpleConverter);
		av.setActivityType(new ActivityTypeConverter().convertToVO(t.getActivityType(), simpleConverter));
		if (!simpleConverter)
			av.setPatient(new PatientConverter().convertToVO(t.getPatient(), simpleConverter));
		return av;
	}
}
