package pl.edu.agh.iosr.pa.rest.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.LocalDateTime;

@XmlRootElement(name = "examination")
@XmlAccessorType(XmlAccessType.NONE)
public class ExaminationVO {
	@XmlAttribute
	private Long id;

	@XmlElementRef
	private PatientVO patient;

	@XmlElement
	private LocalDateTime date;

	@XmlAttribute
	private Integer value;

	@XmlElementRef
	private ExaminationTypeVO examinationType;

	@XmlElementRef
	private VisitVO visit;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PatientVO getPatient() {
		return patient;
	}

	public void setPatient(PatientVO patient) {
		this.patient = patient;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public ExaminationTypeVO getExaminationType() {
		return examinationType;
	}

	public void setExaminationType(ExaminationTypeVO examinationType) {
		this.examinationType = examinationType;
	}

	public VisitVO getVisit() {
		return visit;
	}

	public void setVisit(VisitVO visit) {
		this.visit = visit;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
