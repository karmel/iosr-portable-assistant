package pl.edu.agh.iosr.pa.rest.vo.converters;

import pl.edu.agh.iosr.pa.model.ExaminationType;
import pl.edu.agh.iosr.pa.rest.vo.ExaminationTypeVO;

public class ExaminationTypeConverter extends AbstractConverter<ExaminationType, ExaminationTypeVO> {

	public ExaminationTypeConverter() {
		super(ExaminationType.class, ExaminationTypeVO.class);
	}

	public ExaminationTypeConverter(Class<ExaminationType> tclazz, Class<ExaminationTypeVO> voclazz) {
		super(tclazz, voclazz);
	}

}
