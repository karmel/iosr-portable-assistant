package pl.edu.agh.iosr.pa.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.edu.agh.iosr.pa.model.Examination;

public class ExaminationDao extends BaseDao<Examination> {
	@SuppressWarnings("unchecked")
	public List<Examination> getExaminationsForPatientId(long patientId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Examination.class, "ex");
		criteria.createAlias("ex.patient", "exPatient");
		criteria.add(Restrictions.eq("exPatient.id", patientId));
		criteria.addOrder(Order.asc("ex.date"));
		return criteria.list();
	}
}
