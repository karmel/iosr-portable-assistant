package pl.edu.agh.iosr.pa.rest.vo.jaxbconverters;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.LocalDate;

public class JodaLocalDateXmlAdapter extends XmlAdapter<String, LocalDate> {

	@Override
	public String marshal(LocalDate arg0) throws Exception {
		return arg0.toString();
	}

	@Override
	public LocalDate unmarshal(String arg0) throws Exception {
		return new LocalDate(arg0);
	}

}
