package pl.edu.agh.iosr.pa.util.db;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.Work;
import org.springframework.transaction.annotation.Transactional;

import pl.edu.agh.iosr.pa.dao.DoctorDao;
import pl.edu.agh.iosr.pa.manager.UserManager;
import pl.edu.agh.iosr.pa.model.Doctor;
import pl.edu.agh.iosr.pa.model.Specialization;

@SuppressWarnings("unused")
public class PostgresqlDBInitializeManager implements DBInitializeManager {
	private final String SEQUENCE_CREATE_SQL = "CREATE SEQUENCE HIBERNATE_SEQUENCE";
	private final String SEQUENCE_UPDATE = "ALTER SEQUENCE HIBERNATE_SEQUENCE RESTART WITH 301";// 301
																								// because
																								// maximum
																								// insert
																								// numbers
																								// per
																								// one
																								// table
																								// in
																								// initial
																								// data
																								// is
																								// 300
	private final String SET_DATESTYLE = "SET datestyle TO 'ISO, MDY'";
	private final Logger logger = Logger.getLogger(PostgresqlDBInitializeManager.class);
	private static String CURRENT_DIR = "";// PostgresqlDBInitializeManager.class.getResource(".").getPath();
	private static String INIT_SQLS_DIR = "";// CURRENT_DIR + File.separatorChar
												// + "init" +
												// File.separatorChar;
	private final String INIT_SPECS = "specs.sql";
	private final String INIT_MEDS = "meds.sql";
	private final String INIT_ACTIVITY_TYPES = "atypes.sql";
	private final String INIT_EXAMINATION_TYPES = "exam-type.sql";
	private final String INIT_DOCTORS = "doctors.sql";
	private final String INIT_PATIENTS = "patients.sql";
	private final String INIT_ACTIVITIES = "activities.sql";
	private final String INIT_CRITICAL_VALUES = "critical-values.sql";
	private final String INIT_DIETS = "diets.sql";
	private final String INIT_MEDICAMENT_PATIENTS = "meds-patients.sql";
	private final String INIT_VISITS = "visits.sql";
	private final String INIT_EXAMINATIONS = "examinations.sql";
	private final String INIT_ALERTS = "alerts.sql";

	private DoctorDao doctorDao;
	private SessionFactory sessionFactory;

	@Transactional
	public void init() {
		CURRENT_DIR = PostgresqlDBInitializeManager.class.getResource("init").getPath();
		INIT_SQLS_DIR = CURRENT_DIR + File.separatorChar;
		Session session = sessionFactory.getCurrentSession();
		logger.info("initialize DB for the first time");
		try {
			SQLQuery query = session.createSQLQuery(SEQUENCE_CREATE_SQL);
			query.executeUpdate();
			session.flush();
			try {
				session.close();
			} catch (HibernateException e) {
				logger.debug(e.getMessage(), e);
			} finally {
				session = sessionFactory.openSession();
			}
		} catch (Exception e) {
			// probably sequence already exists
			logger.debug(e.getMessage());
			// but we must get new session, cause current one is "broken"
			session = sessionFactory.openSession();
		}
		try {
			SQLQuery query = session.createSQLQuery(SET_DATESTYLE);
			query.executeUpdate();
			session.flush();
			try {
				session.close();
			} catch (HibernateException e) {
				logger.debug(e.getMessage(), e);
			} finally {
				session = sessionFactory.openSession();
			}
		} catch (Exception e) {
			logger.debug(e.getMessage(), e);
			try {
				session.close();
			} catch (HibernateException e1) {
				logger.debug(e1.getMessage(), e1);
			} finally {
				session = sessionFactory.openSession();
			}
		}

		// init db with example data
		insertExampleData(session);
		// end of init db with example data
		// because manual inserting values into db doesn't affect
		// hibernate_sequence we will hack it and update it manualy
		try {
			SQLQuery query = session.createSQLQuery(SEQUENCE_UPDATE);
			query.executeUpdate();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	private void insertExampleData(Session session) {
		// it iterates over PostgresqlDBInitializeManager fields and takes
		// INIT_* fields' values ( except INIT_SQLS_DIR )
		Session sessToUse = session;
		for (Field f : PostgresqlDBInitializeManager.class.getDeclaredFields()) {
			if (f.getName().startsWith("INIT") && !f.getName().endsWith("DIR")) {
				try {
					executeInitSQLScript(sessToUse, f.get(this).toString());
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
					sessToUse = sessionFactory.openSession();
				}
			}
		}
	}

	@Transactional
	public Serializable exampleSave() {
		logger.info("example object save");

		Doctor doc = new Doctor();
		doc.setAddress("siakis adress");
		doc.setFirstName("Gregory");
		doc.setLastName("House");
		doc.setLogin("md");
		try {
			doc.setPassword(UserManager.encryptPassword("secret"));
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage(), e);
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage(), e);
		}
		doc.setPhone("numerek");

		Specialization doctorSpecialization = new Specialization();
		doctorSpecialization.setName("docSpecs");
		doc.setSpecialization(doctorSpecialization);

		// Patient pat = new Patient();
		// pat.setAddress("siakis adres");
		// pat.setDoctor(doc);
		// pat.setFirstName("Ania");
		// pat.setLastName("Kolasa");
		// pat.setLogin("login");
		// pat.setPassword("asas123");
		// pat.setPhone("123456");
		// ;
		// patientDao.save(pat);
		//
		// Diet diet = new Diet();
		// diet.setDescription("wymagajaca dieta bialkowa");
		// diet.setPatient(pat);
		//

		return doctorDao.save(doc);
	}

	@Transactional(readOnly = true)
	public List<Doctor> exampleFindsBySpecializationName(String specName) {
		return doctorDao.getDoctorsWithSpecializationName(specName);
	}

	@Transactional
	private void executeInitSQLScript(Session session, final String sqlScript) {
		session.doWork(new Work() {
			@Override
			public void execute(Connection arg0) throws SQLException {
				StringBuffer sb = new StringBuffer(INIT_SQLS_DIR);
				sb.append(sqlScript);
				try {
					BufferedReader br = new BufferedReader(new FileReader(new File(sb.toString())));
					String line = null;
					Statement stmt = arg0.createStatement();
					while ((line = br.readLine()) != null) {
						stmt.addBatch(line);
					}
					int[] res = stmt.executeBatch();
					br.close();
				} catch (FileNotFoundException e) {
					logger.error(e.getMessage(), e);
				} catch (IOException e) {
					logger.error(e.getMessage(), e);
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
					throw e;
				}
			}

		});
	}

	public void setDoctorDao(DoctorDao doctorDao) {
		this.doctorDao = doctorDao;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
