package pl.edu.agh.iosr.pa.rest.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement(name = "patients")
@XmlAccessorType(XmlAccessType.NONE)
public class PatientsVO {

	@XmlElementRef
	private List<PatientVO> patients;

	public List<PatientVO> getPatients() {
		return patients;
	}

	public void setPatients(List<PatientVO> patients) {
		this.patients = patients;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
