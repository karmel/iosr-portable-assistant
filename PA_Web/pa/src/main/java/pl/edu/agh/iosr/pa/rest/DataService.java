package pl.edu.agh.iosr.pa.rest;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.Validate;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.codehaus.plexus.util.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.Period;
import org.springframework.transaction.annotation.Transactional;

import pl.edu.agh.iosr.pa.dao.PatientSearchCriteria;
import pl.edu.agh.iosr.pa.manager.AlertManager;
import pl.edu.agh.iosr.pa.manager.HelperManager;
import pl.edu.agh.iosr.pa.manager.TreatmentManager;
import pl.edu.agh.iosr.pa.manager.UserManager;
import pl.edu.agh.iosr.pa.model.Activity;
import pl.edu.agh.iosr.pa.model.ActivityType;
import pl.edu.agh.iosr.pa.model.Alert;
import pl.edu.agh.iosr.pa.model.Diet;
import pl.edu.agh.iosr.pa.model.Doctor;
import pl.edu.agh.iosr.pa.model.Examination;
import pl.edu.agh.iosr.pa.model.ExaminationType;
import pl.edu.agh.iosr.pa.model.Medicament;
import pl.edu.agh.iosr.pa.model.MedicamentPatient;
import pl.edu.agh.iosr.pa.model.Patient;
import pl.edu.agh.iosr.pa.model.Visit;
import pl.edu.agh.iosr.pa.rest.vo.ActivityTypesVO;
import pl.edu.agh.iosr.pa.rest.vo.ActivityVO;
import pl.edu.agh.iosr.pa.rest.vo.AddActivitiesVO;
import pl.edu.agh.iosr.pa.rest.vo.AddDietsVO;
import pl.edu.agh.iosr.pa.rest.vo.AddExaminationVO;
import pl.edu.agh.iosr.pa.rest.vo.AddMedicamentsVO;
import pl.edu.agh.iosr.pa.rest.vo.AddPatientVO;
import pl.edu.agh.iosr.pa.rest.vo.AddVisitVO;
import pl.edu.agh.iosr.pa.rest.vo.AlertsVO;
import pl.edu.agh.iosr.pa.rest.vo.DietVO;
import pl.edu.agh.iosr.pa.rest.vo.ExaminationTypeVO;
import pl.edu.agh.iosr.pa.rest.vo.ExaminationTypesVO;
import pl.edu.agh.iosr.pa.rest.vo.ExaminationVO;
import pl.edu.agh.iosr.pa.rest.vo.MedicamentPatientVO;
import pl.edu.agh.iosr.pa.rest.vo.MedicamentsVO;
import pl.edu.agh.iosr.pa.rest.vo.PatientVO;
import pl.edu.agh.iosr.pa.rest.vo.PatientsVO;
import pl.edu.agh.iosr.pa.rest.vo.VisitVO;
import pl.edu.agh.iosr.pa.rest.vo.converters.ActivityConverter;
import pl.edu.agh.iosr.pa.rest.vo.converters.ActivityTypeConverter;
import pl.edu.agh.iosr.pa.rest.vo.converters.AlertConverter;
import pl.edu.agh.iosr.pa.rest.vo.converters.DietConverter;
import pl.edu.agh.iosr.pa.rest.vo.converters.DoctorConverter;
import pl.edu.agh.iosr.pa.rest.vo.converters.ExaminationConverter;
import pl.edu.agh.iosr.pa.rest.vo.converters.ExaminationTypeConverter;
import pl.edu.agh.iosr.pa.rest.vo.converters.MedicamentConverter;
import pl.edu.agh.iosr.pa.rest.vo.converters.MedicamentPatientConverter;
import pl.edu.agh.iosr.pa.rest.vo.converters.PatientConverter;
import pl.edu.agh.iosr.pa.security.SecurityUtils;

@Path("/data")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.TEXT_PLAIN })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.TEXT_PLAIN })
public class DataService {

	private Logger logger = Logger.getLogger(DataService.class);
	private String GOOGLE_GCM_API_KEY = "key=AIzaSyDK8cf3kMUMSxW0ix_i1mjSZjZJXUvv82Q";
	private String GOOGLE_GCM_URL = "https://android.googleapis.com/gcm/send";
	private String GOOGLE_CLIENT_ID = "42";

	private UserManager userManager;
	private TreatmentManager treatmentManager;
	private HelperManager helperManager;
	private AlertManager alertManager;
	private GCMRegistrationService gcmRegistrationService;

	@PostConstruct
	public void init() {
		if (userManager == null) {
			userManager = (UserManager) SpringContextLoaderListener.getCurrentWebApplicationContext().getBean("userManager");
		}
		logger.debug("DataService initialized");
	}

	@GET
	@Path("/currentUser")
	@Transactional
	public Object getCurrentUser() throws InstantiationException, IllegalAccessException {
		String login = SecurityUtils.getCurrentLogin();
		Object loggedUser = userManager.getDoctorByLogin(login);
		if (loggedUser == null) {
			loggedUser = userManager.getPatientByLogin(login);
			return new PatientConverter().convertToVO((Patient) loggedUser, false);
		}
		return new DoctorConverter().convertToVO((Doctor) loggedUser, false);
	}

	@POST
	@Path("/addActivities")
	public Response addActivities(AddActivitiesVO addActivitiesVO) throws InstantiationException, IllegalAccessException {
		Validate.notNull(addActivitiesVO.getPatientId());
		Patient patient = userManager.getPatientById(addActivitiesVO.getPatientId());
		Validate.notNull(patient);
		Validate.notNull(addActivitiesVO.getActivities());
		Validate.notEmpty(addActivitiesVO.getActivities());
		ActivityConverter aConverter = new ActivityConverter();
		for (ActivityVO avo : addActivitiesVO.getActivities()) {
			Activity av = aConverter.convertFromVO(avo, true);
			Validate.isTrue(av.getId() == null || av.getId() == 0);
			Validate.notNull(av.getStartDate());
			Validate.notNull(av.getEndDate());
			Validate.notNull(av.getFrequency());
			Validate.notNull(avo.getActivityType());
			Validate.notNull(avo.getActivityType().getId());
			ActivityType at = helperManager.findActivityTypeById(avo.getActivityType().getId());
			Validate.notNull(at);
			av.setPatient(patient);
			av.setActivityType(at);
			getTreatmentManager().addActivity(av);
		}
		return Response.ok().build();
	}

	@POST
	@Path("/addSampleActivity")
	public Response addSampleActivity() {
		Activity a = new Activity();
		a.setActivityType(helperManager.findActivityTypeById((long) 1));
		a.setDone(false);
		a.setDuration(new Period(1, 0, 0, 0));
		a.setStartDate(new LocalDate(2014, 5, 10));
		a.setEndDate(new LocalDate(2014, 10, 10));
		a.setFrequency("Czasami");
		a.setPatient(userManager.getPatientById((long) 1));
		Serializable id = getTreatmentManager().addActivity(a);
		return Response.ok(id).build();
	}

	@POST
	@Path("/addDiets")
	public Response addDiets(AddDietsVO addDietsVO) throws InstantiationException, IllegalAccessException {
		Validate.notNull(addDietsVO.getPatientId());
		Patient patient = userManager.getPatientById(addDietsVO.getPatientId());
		Validate.notNull(patient);
		Validate.notNull(addDietsVO.getDiets());
		Validate.notEmpty(addDietsVO.getDiets());
		DietConverter dConverter = new DietConverter();
		for (DietVO dvo : addDietsVO.getDiets()) {
			Diet d = dConverter.convertFromVO(dvo, true);
			Validate.isTrue(d.getId() == null || d.getId() == 0);
			Validate.notNull(d.getDescription());
			Validate.notNull(d.getStartDate());
			Validate.notNull(d.getEndDate());
			d.setPatient(patient);
			getTreatmentManager().addDiet(d);
		}
		return Response.ok().build();
	}

	@POST
	@Path("/addExamination")
	public Response addExamination(AddExaminationVO addExaminationVO) throws InstantiationException, IllegalAccessException {
		Validate.notNull(addExaminationVO.getPatientId());
		Patient patient = userManager.getPatientById(addExaminationVO.getPatientId());
		Validate.notNull(patient);
		Validate.notNull(addExaminationVO.getExamination());
		ExaminationVO exVO = addExaminationVO.getExamination();
		Validate.notNull(exVO.getDate());
		Validate.notNull(exVO.getValue());
		Validate.notNull(exVO.getExaminationType());
		ExaminationTypeVO exTVO = exVO.getExaminationType();
		Validate.notNull(exTVO.getId());
		ExaminationType et = helperManager.findExaminationTypeById(exVO.getExaminationType().getId());
		Validate.notNull(et);
		Examination ex = new ExaminationConverter().convertFromVO(addExaminationVO.getExamination(), true);
		ex.setExaminationType(et);
		ex.setPatient(patient);
		if (exVO.getVisit() != null) {
			Validate.notNull(exVO.getVisit());
			VisitVO vVO = exVO.getVisit();
			Validate.notNull(vVO.getId());
			Visit visit = treatmentManager.findVisitById(vVO.getId());
			Validate.notNull(visit);
			ex.setVisit(visit);
		}
		Serializable id = getTreatmentManager().addExamination(ex);
		return Response.ok(id).build();
	}

	@POST
	@Path("/addUserExamination/{exId}/{exVal}")
	public Serializable addExaminationByPatient(@PathParam("exId") int exId, @PathParam("exVal") int exVal) {
		String login = SecurityUtils.getCurrentLogin();
		Patient pat = userManager.getPatientByLogin(login);
		Examination ex = new Examination();
		ex.setDate(LocalDateTime.now());
		ExaminationType et = helperManager.findExaminationTypeById(exId);
		Validate.notNull(et);
		Validate.notNull(exVal);
		ex.setExaminationType(et);
		ex.setPatient(pat);
		ex.setValue(exVal);
		return treatmentManager.addExamination(ex);
	}

	@POST
	@Path("/fireAlert")
	public Serializable firePanicAlert() throws InstantiationException, IllegalAccessException, ClientProtocolException, IOException {
		String login = SecurityUtils.getCurrentLogin();
		Patient pat = userManager.getPatientByLogin(login);
		Alert alert = new Alert();
		alert.setCause("PANIC PANIC PANIC!!!");
		alert.setPatient(pat);
		alert.setDoctor(pat.getDoctor());
		HttpPost httpPost = new HttpPost(GOOGLE_GCM_URL);
		httpPost.setHeader("Authorization", GOOGLE_GCM_API_KEY);
		httpPost.setHeader("Content-Type", "application/json");
		if (StringUtils.isNotBlank(gcmRegistrationService.getGCM_REGISTRATION_ID())) {
			GOOGLE_CLIENT_ID = gcmRegistrationService.getGCM_REGISTRATION_ID();
		}
		String payload = "{ \"registration_ids\" : [ \"" + GOOGLE_CLIENT_ID + "\" ], \"data\" : { \"patientId\" : " + pat.getId() + ", \"cause\" : \""
				+ alert.getCause() + "\", \"date\" : \"" + LocalDateTime.now().toString() + "\" } }";
		HttpEntity postPayload = new StringEntity(payload);
		httpPost.setEntity(postPayload);
		HttpClient client = HttpClients.createDefault();
		HttpResponse response = client.execute(httpPost);
		logger.debug(response.getStatusLine().toString());
		return alertManager.triggerAlert(alert);
	}

	@POST
	@Path("/addPatient")
	public Response addPatient(AddPatientVO addPatientVO) throws InstantiationException, IllegalAccessException {
		Patient patient = new PatientConverter().convertFromVO(addPatientVO.getPatient(), false);
		Doctor doc = userManager.getDoctorById(addPatientVO.getPatient().getDoctor().getId());
		patient.setDoctor(doc);
		Serializable id = userManager.addPatient(patient);
		return Response.ok(id).build();
	}

	@POST
	@Path("/addMedicaments")
	public Response addMedicaments(AddMedicamentsVO addMedicamentsVO) throws InstantiationException, IllegalAccessException {
		Validate.notNull(addMedicamentsVO.getPatientId());
		Patient patient = userManager.getPatientById(addMedicamentsVO.getPatientId());
		Validate.notNull(patient);
		Validate.notNull(addMedicamentsVO.getMedicaments());
		Validate.notEmpty(addMedicamentsVO.getMedicaments());
		MedicamentPatientConverter mpConverter = new MedicamentPatientConverter();
		for (MedicamentPatientVO mpvo : addMedicamentsVO.getMedicaments()) {
			MedicamentPatient mp = mpConverter.convertFromVO(mpvo, true);
			Validate.notNull(mpvo.getMedicament());
			Validate.notNull(mpvo.getMedicament().getId());
			Medicament m = treatmentManager.findMedicamentById(mpvo.getMedicament().getId());
			Validate.notNull(m);
			mp.setMedicament(m);
			mp.setPatient(patient);
			getTreatmentManager().addMedicamentPatient(mp);
		}
		return Response.ok().build();
	}

	@POST
	@Path("/searchPatient")
	@Transactional
	public PatientsVO searchPatient(PatientVO patientCriteria) throws InstantiationException, IllegalAccessException {
		PatientSearchCriteria psc = new PatientSearchCriteria();
		if (StringUtils.isNotBlank(patientCriteria.getLogin())) {
			psc.setLogin(patientCriteria.getLogin());
		}
		if (StringUtils.isNotBlank(patientCriteria.getAddress())) {
			psc.setAddress(patientCriteria.getAddress());
		}
		if (StringUtils.isNotBlank(patientCriteria.getFirstName())) {
			psc.setFirstName(patientCriteria.getFirstName());
		}
		if (StringUtils.isNotBlank(patientCriteria.getLastName())) {
			psc.setLastName(patientCriteria.getLastName());
		}
		List<Patient> matchedPatients = userManager.searchPatients(psc);
		PatientsVO pvo = new PatientsVO();
		pvo.setPatients(new ArrayList<PatientVO>());
		if (matchedPatients != null && matchedPatients.size() > 0) {
			PatientConverter pc = new PatientConverter();
			for (Patient p : matchedPatients) {
				pvo.getPatients().add(pc.convertToVO(p, true));
			}
		}
		return pvo;
	}

	@POST
	@Path("/addVisit")
	public Response addVisit(AddVisitVO addVisitVO) {
		Validate.notNull(addVisitVO.getDate());
		Validate.notNull(addVisitVO.getDoctorId());
		Doctor doctor = userManager.getDoctorById(addVisitVO.getDoctorId());
		Validate.notNull(doctor);
		Validate.notNull(addVisitVO.getPatientId());
		Patient patient = userManager.getPatientById(addVisitVO.getPatientId());
		Validate.notNull(patient);
		Serializable id = getTreatmentManager().addVisit(addVisitVO.getDate(), doctor, patient);
		return Response.ok(id).build();
	}

	@GET
	@Path("/medicaments")
	@Transactional
	public MedicamentsVO getAllMedicaments() throws InstantiationException, IllegalAccessException {
		List<Medicament> meds = treatmentManager.getAllMedicaments();
		MedicamentsVO medsVO = new MedicamentsVO();
		medsVO.setMedicaments(new MedicamentConverter().convertListToVOs(meds, false));
		return medsVO;
	}

	@GET
	@Path("/alerts")
	@Transactional
	public AlertsVO getAllAlerts() throws InstantiationException, IllegalAccessException {
		List<Alert> allAlerts = alertManager.getAlerts();
		AlertsVO allAlertsVO = new AlertsVO();
		allAlertsVO.setAlerts(new AlertConverter().convertListToVOs(allAlerts, false));
		return allAlertsVO;
	}

	@GET
	@Path("/activityTypes")
	public ActivityTypesVO getActivityTypes() throws InstantiationException, IllegalAccessException {
		List<ActivityType> aTypes = helperManager.getActivityTypes();
		ActivityTypesVO aTypesVO = new ActivityTypesVO();
		aTypesVO.setActivityTypes(new ActivityTypeConverter().convertListToVOs(aTypes, true));
		return aTypesVO;
	}

	@GET
	@Path("/examinationTypes")
	public ExaminationTypesVO getExaminationTypes() throws InstantiationException, IllegalAccessException {
		List<ExaminationType> eTypes = helperManager.getExaminationTypes();
		ExaminationTypesVO eTypesVO = new ExaminationTypesVO();
		eTypesVO.setExaminationTypes(new ExaminationTypeConverter().convertListToVOs(eTypes, true));
		return eTypesVO;

	}

	public UserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public TreatmentManager getTreatmentManager() {
		return treatmentManager;
	}

	public void setTreatmentManager(TreatmentManager treatmentManager) {
		this.treatmentManager = treatmentManager;
	}

	public HelperManager getHelperManager() {
		return helperManager;
	}

	public void setHelperManager(HelperManager helperManager) {
		this.helperManager = helperManager;
	}

	public AlertManager getAlertManager() {
		return alertManager;
	}

	public void setAlertManager(AlertManager alertManager) {
		this.alertManager = alertManager;
	}

	public GCMRegistrationService getGcmRegistrationService() {
		return gcmRegistrationService;
	}

	public void setGcmRegistrationService(GCMRegistrationService gcmRegistrationService) {
		this.gcmRegistrationService = gcmRegistrationService;
	}
}
