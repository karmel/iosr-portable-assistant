package pl.edu.agh.iosr.pa.security;

import org.springframework.security.core.AuthenticationException;

/**
 * This exception is throw in case of a not activated user trying to
 * authenticate.
 */
public class UserNotActivatedException extends AuthenticationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3041226318069459215L;

	public UserNotActivatedException(String message) {
		super(message);
	}

	public UserNotActivatedException(String message, Throwable t) {
		super(message, t);
	}
}
