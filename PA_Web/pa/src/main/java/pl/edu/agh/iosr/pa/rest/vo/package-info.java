/**
 * 
 */
/**
 * @author Aral
 *
 */
@XmlJavaTypeAdapters({ @XmlJavaTypeAdapter(type = LocalDate.class, value = JodaLocalDateXmlAdapter.class),
		@XmlJavaTypeAdapter(type = Period.class, value = JodaPeriodXmlAdapter.class),
		@XmlJavaTypeAdapter(type = org.joda.time.LocalDateTime.class, value = pl.edu.agh.iosr.pa.rest.vo.jaxbconverters.JodaLocalDateTimeXmlAdapter.class) })
package pl.edu.agh.iosr.pa.rest.vo;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

import org.joda.time.LocalDate;
import org.joda.time.Period;

import pl.edu.agh.iosr.pa.rest.vo.jaxbconverters.JodaLocalDateXmlAdapter;
import pl.edu.agh.iosr.pa.rest.vo.jaxbconverters.JodaPeriodXmlAdapter;

