package pl.edu.agh.iosr.pa.rest.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "addDiets")
@XmlAccessorType(XmlAccessType.NONE)
public class AddDietsVO {

	@XmlElementRef
	private List<DietVO> diets;

	@XmlAttribute
	private Long patientId;

	public List<DietVO> getDiets() {
		return diets;
	}

	public void setDiets(List<DietVO> diets) {
		this.diets = diets;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

}
