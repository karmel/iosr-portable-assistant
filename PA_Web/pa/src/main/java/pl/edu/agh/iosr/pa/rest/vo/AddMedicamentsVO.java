package pl.edu.agh.iosr.pa.rest.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "addMedicaments")
@XmlAccessorType(XmlAccessType.NONE)
public class AddMedicamentsVO {

	@XmlElementRef
	private List<MedicamentPatientVO> medicaments;

	@XmlAttribute
	private Long patientId;

	public List<MedicamentPatientVO> getMedicaments() {
		return medicaments;
	}

	public void setMedicaments(List<MedicamentPatientVO> medicaments) {
		this.medicaments = medicaments;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

}
