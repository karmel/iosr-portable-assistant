package pl.edu.agh.iosr.pa.rest.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "examinationTypes")
@XmlAccessorType(XmlAccessType.NONE)
public class ExaminationTypesVO {

	@XmlElementRef
	private List<ExaminationTypeVO> examinationTypes;

	public List<ExaminationTypeVO> getExaminationTypes() {
		return examinationTypes;
	}

	public void setExaminationTypes(List<ExaminationTypeVO> examinationTypes) {
		this.examinationTypes = examinationTypes;
	}
}
