package pl.edu.agh.iosr.pa.rest.vo.converters;

import pl.edu.agh.iosr.pa.model.Diet;
import pl.edu.agh.iosr.pa.rest.vo.DietVO;

public class DietConverter extends AbstractConverter<Diet, DietVO> {

	public DietConverter() {
		super(Diet.class, DietVO.class);
	}

	public DietConverter(Class<Diet> tclazz, Class<DietVO> voclazz) {
		super(tclazz, voclazz);
	}

	@Override
	public Diet convertFromVO(DietVO vo, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		Diet d = super.convertFromVO(vo, simpleConverter);
		if (!simpleConverter) {
			d.setPatient(new PatientConverter().convertFromVO(vo.getPatient(), simpleConverter));
		}
		return d;
	}

	@Override
	public DietVO convertToVO(Diet t, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		DietVO vo = super.convertToVO(t, simpleConverter);
		if (!simpleConverter) {
			vo.setPatient(new PatientConverter().convertToVO(t.getPatient(), simpleConverter));
		}
		return vo;
	}

}
