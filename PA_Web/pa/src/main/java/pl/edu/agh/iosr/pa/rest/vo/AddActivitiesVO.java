package pl.edu.agh.iosr.pa.rest.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "addActivities")
@XmlAccessorType(XmlAccessType.NONE)
public class AddActivitiesVO {

	@XmlElementRef
	private List<ActivityVO> activities;

	@XmlAttribute
	private Long patientId;

	public List<ActivityVO> getActivities() {
		return activities;
	}

	public void setActivities(List<ActivityVO> activities) {
		this.activities = activities;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}
}
