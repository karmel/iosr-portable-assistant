package pl.edu.agh.iosr.pa.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import pl.edu.agh.iosr.pa.model.Doctor;

public class DoctorDao extends BaseDao<Doctor> {
	@SuppressWarnings("unchecked")
	public List<Doctor> getDoctorsWithSpecializationName(String specializationName) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Doctor.class, "doc");
		criteria.createAlias("doc.specialization", "docSpec");

		criteria.add(Restrictions.eq("docSpec.name", specializationName));

		return criteria.list();
	}

	public Doctor login(String login, String password) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Doctor.class);

		criteria.add(Restrictions.eq("login", login));
		criteria.add(Restrictions.eq("password", password));

		return (Doctor) criteria.uniqueResult();
	}

	public Doctor findByLogin(String login) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Doctor.class);
		criteria.add(Restrictions.eq("login", login));

		return (Doctor) criteria.uniqueResult();
	}
}
