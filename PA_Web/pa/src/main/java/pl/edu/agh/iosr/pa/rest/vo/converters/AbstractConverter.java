package pl.edu.agh.iosr.pa.rest.vo.converters;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

public class AbstractConverter<T, VO> {

	private static final Logger log = Logger.getLogger(AbstractConverter.class);

	private Class<T> modelObject;
	private Class<VO> valueObject;

	public AbstractConverter(Class<T> tclazz, Class<VO> voclazz) {
		modelObject = tclazz;
		valueObject = voclazz;
	}

	public VO convertToVO(T t, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		VO vo = valueObject.newInstance();
		try {
			BeanUtils.copyProperties(t, vo, Utils.getNullPropertyNames(t));
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}
		return vo;
	}

	public T convertFromVO(VO vo, boolean SimpleConverter) throws InstantiationException, IllegalAccessException {
		T t = modelObject.newInstance();
		try {
			BeanUtils.copyProperties(vo, t, Utils.getNullPropertyNames(vo));
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}
		return t;
	}

	public List<VO> convertListToVOs(List<T> t_list, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		List<VO> vos = new LinkedList<VO>();
		for (T t : t_list) {
			vos.add(convertToVO(t, simpleConverter));
		}
		return vos;
	}

	public List<T> convertListFromVOs(List<VO> vo_list, boolean simpleConverter) throws InstantiationException, IllegalAccessException {
		List<T> t_list = new LinkedList<T>();
		for (VO vo : vo_list) {
			t_list.add(convertFromVO(vo, simpleConverter));
		}
		return t_list;
	}
}
