package pl.edu.agh.iosr.pa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "IOSR_CRV")
public class CriticalValue {
	@Id
	@Column(name = "CRV_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CRV_PAT_ID")
	private Patient patient;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CRV_EXT_ID")
	private ExaminationType examinationType;

	@Column(name = "CRV_MIN_VAL")
	private Long minValue;

	@Column(name = "CRV_MAX_VAL")
	private Long maxValue;

	public CriticalValue() {
	}

	public Long getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Long max_value) {
		this.maxValue = max_value;
	}

	public Long getMinValue() {
		return minValue;
	}

	public void setMinValue(Long min_value) {
		this.minValue = min_value;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ExaminationType getExaminationType() {
		return examinationType;
	}

	public void setExaminationType(ExaminationType examinationType) {
		this.examinationType = examinationType;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patients) {
		this.patient = patients;
	}

}
