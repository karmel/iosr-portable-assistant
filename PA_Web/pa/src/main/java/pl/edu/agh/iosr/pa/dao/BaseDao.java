package pl.edu.agh.iosr.pa.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class BaseDao<T> {
	protected SessionFactory sessionFactory;
	
	public Serializable save(T object) {
		Session session = sessionFactory.getCurrentSession();
		return session.save(object);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findAll(Class<T> what) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(what);
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public T findById(Class<T> what, Serializable id) {
		Session session = sessionFactory.getCurrentSession();
		return (T) session.get(what, id);
	}
	
	public void update(T object){
		Session session = sessionFactory.getCurrentSession();
		session.update(object);
	}
	
	public void delete(T object){
		Session session = sessionFactory.getCurrentSession();
		session.delete(object);
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	
}
