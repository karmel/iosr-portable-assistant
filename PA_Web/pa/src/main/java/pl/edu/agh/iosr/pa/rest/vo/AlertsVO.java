package pl.edu.agh.iosr.pa.rest.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "alert")
@XmlAccessorType(XmlAccessType.NONE)
public class AlertsVO {

	@XmlElementRef
	private List<AlertVO> alerts;

	public List<AlertVO> getAlerts() {
		return alerts;
	}

	public void setAlerts(List<AlertVO> alerts) {
		this.alerts = alerts;
	}
}
