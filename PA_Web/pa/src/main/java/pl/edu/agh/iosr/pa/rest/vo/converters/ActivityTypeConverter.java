package pl.edu.agh.iosr.pa.rest.vo.converters;

import pl.edu.agh.iosr.pa.model.ActivityType;
import pl.edu.agh.iosr.pa.rest.vo.ActivityTypeVO;

public class ActivityTypeConverter extends AbstractConverter<ActivityType, ActivityTypeVO> {

	public ActivityTypeConverter() {
		super(ActivityType.class, ActivityTypeVO.class);
	}

	public ActivityTypeConverter(Class<ActivityType> tclazz, Class<ActivityTypeVO> voclazz) {
		super(tclazz, voclazz);
	}

}
