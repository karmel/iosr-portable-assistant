package pl.edu.agh.iosr.pa.rest.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "addExamination")
@XmlAccessorType(XmlAccessType.NONE)
public class AddExaminationVO {

	@XmlElementRef
	private ExaminationVO examination;

	@XmlAttribute
	private Long patientId;

	public ExaminationVO getExamination() {
		return examination;
	}

	public void setExamination(ExaminationVO examination) {
		this.examination = examination;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

}
