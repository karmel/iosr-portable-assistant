package pl.edu.agh.iosr.pa.rest;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.Validate;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import pl.edu.agh.iosr.pa.manager.TreatmentManager;
import pl.edu.agh.iosr.pa.manager.UserManager;
import pl.edu.agh.iosr.pa.model.Doctor;
import pl.edu.agh.iosr.pa.model.Specialization;
import pl.edu.agh.iosr.pa.rest.vo.DoctorVO;
import pl.edu.agh.iosr.pa.rest.vo.DoctorsVO;
import pl.edu.agh.iosr.pa.rest.vo.converters.DoctorConverter;

@Path("/doctor")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.TEXT_PLAIN })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.TEXT_PLAIN })
public class DoctorService {
	private Logger log = Logger.getLogger(DoctorService.class);

	private UserManager userManager;
	private TreatmentManager treatmentManager;

	@PostConstruct
	public void init() {
		if (userManager == null) {
			userManager = (UserManager) SpringContextLoaderListener.getCurrentWebApplicationContext().getBean("userManager");
		}
		log.debug("DoctorService initialized");
	}

	@GET
	@Transactional
	public DoctorsVO getAllDoctors() throws InstantiationException, IllegalAccessException {
		List<DoctorVO> allDoctors = new DoctorConverter().convertListToVOs(getUserManager().getAllDoctors(), false);
		DoctorsVO docs = new DoctorsVO();
		docs.setDoctors(allDoctors);
		return docs;
	}

	@GET
	@Path("/{login}")
	@Transactional
	public DoctorVO getDoctorByLogin(@PathParam(value = "login") String login) throws InstantiationException, IllegalAccessException {
		Validate.notBlank(login);
		Doctor doctor = getUserManager().getDoctorByLogin(login);
		Validate.notNull(doctor);
		DoctorVO doc = new DoctorConverter().convertToVO(doctor, false);
		return doc;
	}

	@GET
	@Path("/test")
	public DoctorVO getTestDoctor() throws Exception {
		Doctor doc1 = new Doctor();
		doc1.setId((long) 1);
		doc1.setAddress("siakis address");
		doc1.setFirstName("Anna");
		doc1.setLastName("Glocka");
		doc1.setLogin("lpankaz");
		doc1.setPassword("zaaaaaba");
		doc1.setPhone("123456677");
		Specialization ds = new Specialization();
		ds.setId((long) 1);
		ds.setName("alergolog");
		doc1.setSpecialization(ds);
		DoctorVO docVO = new DoctorVO();
		try {
			docVO = new DoctorConverter().convertToVO(doc1, false);
			return docVO;
		} catch (Exception e) {
			log.error(e);
			throw e;
		}
	}

	public UserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public TreatmentManager getTreatmentManager() {
		return treatmentManager;
	}

	public void setTreatmentManager(TreatmentManager treatmentManager) {
		this.treatmentManager = treatmentManager;
	}
}
