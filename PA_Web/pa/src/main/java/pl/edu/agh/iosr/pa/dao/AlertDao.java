package pl.edu.agh.iosr.pa.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import pl.edu.agh.iosr.pa.model.Alert;

public class AlertDao extends BaseDao<Alert>{
	@SuppressWarnings("unchecked")
	public List<Alert> getAlertsForPatientId(long patientId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Alert.class, "alert");
		criteria.createAlias("alert.patient", "alertPatient");
		criteria.add(Restrictions.eq("alertPatient.id", patientId));
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Alert> getAlertsForDoctorId(long doctorId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Alert.class, "alert");
		criteria.createAlias("alert.doctor", "alertDoctor");
		criteria.add(Restrictions.eq("alertDoctor.id", doctorId));
		return criteria.list();
	}
}
