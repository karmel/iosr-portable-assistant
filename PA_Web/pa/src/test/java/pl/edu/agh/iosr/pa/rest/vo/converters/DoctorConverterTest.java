package pl.edu.agh.iosr.pa.rest.vo.converters;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.junit.Test;

import pl.edu.agh.iosr.pa.model.Doctor;
import pl.edu.agh.iosr.pa.model.Specialization;
import pl.edu.agh.iosr.pa.rest.vo.DoctorVO;

public class DoctorConverterTest {

	private static Logger log = Logger.getLogger(DoctorConverterTest.class);

	@Test
	public void shouldCopyPropertiesGeneric() {
		Doctor doc1 = new Doctor();
		doc1.setId((long) 1);
		doc1.setAddress("siakis address");
		doc1.setFirstName("Anna");
		doc1.setLastName("Glocka");
		doc1.setLogin("lpankaz");
		doc1.setPassword("zaaaaaba");
		doc1.setPhone("123456677");
		Specialization ds = new Specialization();
		ds.setId((long) 1);
		ds.setName("alergolog");
		doc1.setSpecialization(ds);
		DoctorVO docVO = new DoctorVO();
		try {
			docVO = new DoctorConverter().convertToVO(doc1, false);
			assertNotNull(docVO.getSpecialization());
			assertTrue(docVO.getSpecialization().getName().equals(doc1.getSpecialization().getName()));
			System.out.println(docVO);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			fail(e.getMessage());
		}
	}
}
