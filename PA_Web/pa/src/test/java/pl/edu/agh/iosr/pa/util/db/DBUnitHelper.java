package pl.edu.agh.iosr.pa.util.db;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

import org.dbunit.Assertion;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.SortedDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.hsqldb.HsqldbDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;

public class DBUnitHelper {

	private static DatabaseConnection dbUnitConnection;

	public static void initDB(String fileName) throws Exception {
		FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
		builder.setColumnSensing(true);
		InputStream is = DBUnitHelper.class.getClassLoader().getResourceAsStream("integration/" + fileName);
		IDataSet dataSet = builder.build(is);
		DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnection, dataSet);
	}

	public static void initialize(Connection connection)
			throws DatabaseUnitException {
		dbUnitConnection = new DatabaseConnection(connection);
		DatabaseConfig cfg = dbUnitConnection.getConfig();
		
		cfg.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new HsqldbDataTypeFactory());
	}

	public static void assertDB(String fileName) throws SQLException, DatabaseUnitException {
		FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
		builder.setColumnSensing(true);
		InputStream is = DBUnitHelper.class.getClassLoader().getResourceAsStream("integration/" + fileName);
		IDataSet dataSet = builder.build(is);
		
		SortedDataSet expected = new SortedDataSet(dataSet);
		
		IDataSet tmpDataset = dbUnitConnection.createDataSet(expected.getTableNames());
		SortedDataSet actual = new SortedDataSet(tmpDataset);
		
		Assertion.assertEquals(expected, actual);
	}

}
