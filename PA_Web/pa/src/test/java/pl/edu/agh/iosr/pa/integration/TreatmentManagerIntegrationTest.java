package pl.edu.agh.iosr.pa.integration;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import pl.edu.agh.iosr.pa.manager.TreatmentManager;
import pl.edu.agh.iosr.pa.manager.UserManager;
import pl.edu.agh.iosr.pa.model.Doctor;
import pl.edu.agh.iosr.pa.model.Patient;
import pl.edu.agh.iosr.pa.util.db.DBUnitHelper;

public class TreatmentManagerIntegrationTest extends AbstractIntegrationTest {

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@Test
	@Transactional
	public void addVisit() throws Exception {
		DBUnitHelper.initDB("HsqldbDBInitializeManager-addVisit-init.xml");

		UserManager userManager = (UserManager) ctx.getBean("userManager");

		TreatmentManager treatmentManager = (TreatmentManager) ctx.getBean("treatmentManager");

		List<Doctor> docs = userManager.getAllDoctors();
		List<Patient> patients = userManager.getAllPatients();

		assertEquals(1, patients.size());
		assertEquals(1, docs.size());

		LocalDateTime date;

		// 2014-05-24 12:00:00
		date = new LocalDateTime(2014, 06, 24, 12, 0, 0);

		// when
		treatmentManager.addVisit(date, docs.get(0), patients.get(0));

		// then
		DBUnitHelper.assertDB("HsqldbDBInitializeManager-addVisit-expected.xml");

	}

}
