package pl.edu.agh.iosr.pa.util.db;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

public class HsqldbDBInitializeManager implements DBInitializeManager{

	Logger logger = Logger.getLogger(HsqldbDBInitializeManager.class);
	private SessionFactory sessionFactory;
	
	@Transactional
	public void init() {
		logger.info("starting hsqldb initialization");
		
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
		
	}
	
}
