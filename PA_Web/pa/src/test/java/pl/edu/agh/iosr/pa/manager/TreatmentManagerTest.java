package pl.edu.agh.iosr.pa.manager;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.Serializable;

import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;

import pl.edu.agh.iosr.pa.dao.VisitDao;
import pl.edu.agh.iosr.pa.model.Doctor;
import pl.edu.agh.iosr.pa.model.Patient;
import pl.edu.agh.iosr.pa.model.Visit;

public class TreatmentManagerTest {
	private TreatmentManager underTest;
	private VisitDao mockVisitDao;

	@Before
	public void setUp() {
		underTest = new TreatmentManager();

		mockVisitDao = mock(VisitDao.class);
	}

	@Test
	public void should_add_visist() {
		// given
		underTest.setVisitDao(mockVisitDao);

		final LocalDateTime date = new LocalDateTime();
		final Doctor doctor = new Doctor();
		final Patient patient = new Patient();

		Serializable someObject = 10L;
		when(mockVisitDao.save((Visit) anyObject())).thenReturn(someObject);

		// when
		Serializable res = underTest.addVisit(date, doctor, patient);

		// then
		verify(mockVisitDao).save(argThat(new ArgumentMatcher<Visit>() {
			@Override
			public boolean matches(Object actual) {
				Visit actualVisit = (Visit) actual;
				assertSame(date, actualVisit.getDate());
				assertSame(doctor, actualVisit.getDoctor());
				assertSame(patient, actualVisit.getPatient());

				return true;
			}
		}));

		assertSame(someObject, res);
	}
}
