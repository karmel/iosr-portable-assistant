package pl.edu.agh.iosr.pa.manager;

import static org.junit.Assert.assertEquals;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import org.junit.Before;
import org.junit.Test;

public class UserManagerTest {

	private UserManager underTest;

	@Before
	public void setUp() {
		underTest = new UserManager();
	}

	@Test
	public void shouldEncryptPassword() {
		// given
		String text = "z1a2b3a4";
		String expected = "8c6eb95fd99cc10d3180da7f00120d12863101a235ad61a73875c66e4e46eb39";
		// when
		try {
			@SuppressWarnings("static-access")
			String encrypted = underTest.encryptPassword(text);
			// then
			assertEquals(expected, encrypted);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

}
