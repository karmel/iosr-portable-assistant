package pl.edu.agh.iosr.pa.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import pl.edu.agh.iosr.pa.manager.UserManager;
import pl.edu.agh.iosr.pa.model.Doctor;
import pl.edu.agh.iosr.pa.model.Patient;
import pl.edu.agh.iosr.pa.util.db.DBUnitHelper;

public class UserManagerIntegrationTest extends AbstractIntegrationTest{
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
	}
	
	@Test
	@Transactional
	public void addPatientAndLogin() throws Exception {
		DBUnitHelper.initDB("HsqldbDBInitializeManager-addPatient-init.xml");
		UserManager userManager = (UserManager) ctx.getBean("userManager");
		
		List<Doctor> docs = userManager.getAllDoctors();
		

		Patient patient1 = new Patient();
		patient1.setAddress("adresik1");
		patient1.setDoctor(docs.get(0));
		patient1.setFirstName("Ania");
		patient1.setLastName("Aniowa");
		patient1.setLogin("lpankaz");
		patient1.setPassword("zaba123");
		patient1.setPhone("asas");
		userManager.addPatient(patient1);
		
		List<Patient> patients = userManager.getAllPatients();
		assertEquals(1, patients.size());
		
		
		Patient patient2 = new Patient();
		patient2.setAddress("adresik2");
		patient2.setDoctor(docs.get(0));
		patient2.setFirstName("Aniaaa");
		patient2.setLastName("Aniowa");
		patient2.setLogin("lpankaz");
		patient2.setPassword("zaba1234");
		patient2.setPhone("asas");
		//should not add patient with same login
		try{
			userManager.addPatient(patient2);
		}catch(ConstraintViolationException e){
			assertNotNull(e);
		}
		
		
		//when
		Patient found = userManager.getPatientByLogin("lpankaz");
		assertEquals(found.getPassword(), "d06f405a3d21248bc93cee933a4ff86977d5a85a588406903e115f324889c3b4");
		
		Patient logged = userManager.loginPatient("lpankaz", "zaba123");
		assertNotNull(logged);
		
		Patient notlogged = userManager.loginPatient("lpankaz", "pass");
		assertNull(notlogged);
		
		Doctor loggedDoctor = userManager.loginDoctor("md", "secret");
		assertNotNull(loggedDoctor);
		
		Doctor notLoggedDoctor = userManager.loginDoctor("md", "aaaa");
		assertNull(notLoggedDoctor);
		
	}
	

}
