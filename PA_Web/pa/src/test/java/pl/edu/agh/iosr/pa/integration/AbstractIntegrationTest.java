package pl.edu.agh.iosr.pa.integration;

import java.sql.Connection;

import javax.sql.DataSource;

import org.junit.Before;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pl.edu.agh.iosr.pa.util.db.DBUnitHelper;

public class AbstractIntegrationTest {
	protected ClassPathXmlApplicationContext ctx;

	@Before
	public void setUp() throws Exception {
		ctx  = new ClassPathXmlApplicationContext("applicationContext.xml", "applicationContext-test-configuration.xml");
		
		DataSource ds = (DataSource) ctx.getBean("dataSource");
		Connection connection = ds.getConnection();
		
		DBUnitHelper.initialize(connection);
	}
}
