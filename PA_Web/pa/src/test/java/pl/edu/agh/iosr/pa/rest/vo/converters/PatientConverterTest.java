package pl.edu.agh.iosr.pa.rest.vo.converters;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.junit.Test;

import pl.edu.agh.iosr.pa.model.Patient;
import pl.edu.agh.iosr.pa.rest.vo.PatientVO;

public class PatientConverterTest {
	private static Logger log = Logger.getLogger(PatientConverterTest.class);

	@Test
	public void shouldCopyPropertiesGeneric() {
		Patient pat1 = new Patient();
		pat1.setAddress("siakis address");
		pat1.setFirstName("Anna");
		pat1.setLastName("Glocka");
		pat1.setLogin("lpankaz");
		pat1.setPassword("zaaaaaba");
		pat1.setPhone("123456677");
		PatientVO patVO = new PatientVO();
		try {
			patVO = new PatientConverter().convertToVO(pat1, true);
		} catch (InstantiationException | IllegalAccessException e) {
			log.debug(e.getMessage(), e);
		}
		System.out.println(patVO);

	}

	@Test
	public void shouldCopyPropertiesGenericFromVO() {
		PatientVO pat1 = new PatientVO();
		pat1.setAddress("siakis address");
		pat1.setFirstName("Anna");
		pat1.setLastName("Glocka");
		pat1.setLogin("lpankaz");
		pat1.setPassword("zaaaaaba");
		pat1.setPhone("123456677");
		Patient pat = new Patient();
		try {
			pat = new PatientConverter().convertFromVO(pat1, true);
		} catch (InstantiationException | IllegalAccessException e) {
			log.debug(e.getMessage(), e);
		}
		System.out.println(ReflectionToStringBuilder.toString(pat));

	}
}
