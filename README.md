# Dokumentacja procesowa

* [Wizja projektu](https://bitbucket.org/karmel/iosr-portable-assistant/wiki/Wizja projektu)
* [Plan prac](https://bitbucket.org/karmel/iosr-portable-assistant/wiki/Plan prac)
* [Minutki ze spotkań](https://bitbucket.org/karmel/iosr-portable-assistant/wiki/Minutki ze spotkań)
* [Role w zespole i podział prac](https://bitbucket.org/karmel/iosr-portable-assistant/wiki/Role w zespole)
* [Kaizen - elementy niezrealizowane i możliwości rozwoju](https://bitbucket.org/karmel/iosr-portable-assistant/wiki/Kaizen)

# Specyfikacja wymagań #
* [Specyfikacja wymagań funkcjonalnych](https://bitbucket.org/karmel/iosr-portable-assistant/wiki/Specyfikacja wymagań funkcjonalnych) 
* [Specyfikacja wymagań niefunkcjonalnych](https://bitbucket.org/karmel/iosr-portable-assistant/wiki/Specyfikacja wymagań niefunkcjonalnych)

# Dokumentacja techniczna

* [Architektura](https://bitbucket.org/karmel/iosr-portable-assistant/wiki/Opis architektury)
* [Model danych](https://bitbucket.org/karmel/iosr-portable-assistant/wiki/Model danych)
* [Specyfikacja API](https://bitbucket.org/karmel/iosr-portable-assistant/wiki/Specyfikacja API)
* Struktura projektu 
    * [Stos technologiczny](https://bitbucket.org/karmel/iosr-portable-assistant/wiki/Stos technologiczny)
    * [Struktura repozytorium](https://bitbucket.org/karmel/iosr-portable-assistant/wiki/Struktura repozytorium)
* [Przygotowanie środowiska deweloperskiego](https://bitbucket.org/karmel/iosr-portable-assistant/wiki/Przygotowanie środowiska deweloperskiego)
* [Dokumentacja testów wydajnościowych](https://bitbucket.org/karmel/iosr-portable-assistant/wiki/Dokumentacja testów wydajnościowych)
